/*
 * Created by SharpDevelop.
 * User: john
 * Date: 01/08/2009
 * Time: 15:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Windows.Forms;
using System.Xml;

namespace XmlAppSettings
{
	public static class Settings
	{
		static XmlDocument xmlDocument;
		static XmlDocument XmlDocument {
			get {
				if(xmlDocument==null) {
					xmlDocument = new XmlDocument();
					try {xmlDocument.Load(documentPath);}
					catch {xmlDocument.LoadXml("<settings></settings>");}
				}
				return xmlDocument;
			}
		}
		
		static string documentPath = Application.StartupPath + "//AppSettings.xml";

		static public int GetSetting(string xPath, int defaultValue)
		{
			return Convert.ToInt16(GetSetting(xPath, Convert.ToString(defaultValue)));
		}

		static public void PutSetting(string xPath, int value)
		{
			PutSetting(xPath, Convert.ToString(value));
		}

		static public string GetSetting(string xPath,  string defaultValue)
		{
			XmlNode xmlNode = XmlDocument.SelectSingleNode("settings/" + xPath );
			if (xmlNode != null) {return xmlNode.InnerText;}
			else { return defaultValue;}
		}

		static public void PutSetting(string xPath,  string value)
		{
			XmlNode xmlNode = XmlDocument.SelectSingleNode("settings/" + xPath);
			if (xmlNode == null) { xmlNode = createMissingNode("settings/" + xPath); }
			xmlNode.InnerText = value;
			XmlDocument.Save(documentPath);
		}

		static private XmlNode createMissingNode(string xPath)
		{
			string[] xPathSections = xPath.Split('/');
			string currentXPath = "";
			XmlNode testNode = null;
			XmlNode currentNode = XmlDocument.SelectSingleNode("settings");
			foreach (string xPathSection in xPathSections)
			{ currentXPath += xPathSection;
				testNode = XmlDocument.SelectSingleNode(currentXPath);
				if (testNode == null)
				{
					currentNode.InnerXml += "<" +
						xPathSection + "></" +
						xPathSection + ">";
				}
				currentNode = XmlDocument.SelectSingleNode(currentXPath);
				currentXPath += "/";
			}
			return currentNode;
		}
	}
}