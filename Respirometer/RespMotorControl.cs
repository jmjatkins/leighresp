/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using LeighDAQclient;
using WeifenLuo.WinFormsUI.Docking;

namespace Respirometer
{
	/// Description of RespMotorControl.
	public partial class RespMotorControl : DockContent, INotifyPropertyChanged
	{
		const double maxVoltageSetting = 10.0;
		
		public RespMotorControl()
		{
			const int DAQ_UPDATE_DELAY = 1000;
			HideOnClose = true;
			Timer daqUpdateTimer = new Timer();
			InitializeComponent();
			TabText = "Flume";
			InitUI();
			try {
				CalibrationFactor0 =  (double) ((double)XmlAppSettings.Settings.GetSetting("FlowCalibrationFactor0", 100 ) / 10000);
				CalibrationFactor1 =  (double) ((double)XmlAppSettings.Settings.GetSetting("FlowCalibrationFactor1", 100 ) / 10000);
				CalibrationFactor2 =  (double) ((double)XmlAppSettings.Settings.GetSetting("FlowCalibrationFactor2", 100 ) / 10000);
				SetPoint =  (double) ((double)XmlAppSettings.Settings.GetSetting("FlowSetPoint", 100 ) / 100);
				FishLength =(double) ((double)XmlAppSettings.Settings.GetSetting("FishLength", 2 ) / 1000);
				FlowControlEnabled = Convert.ToBoolean(  XmlAppSettings.Settings.GetSetting("FlowControlEnabled", 0) );
				FishDepth = (double) ((double)XmlAppSettings.Settings.GetSetting("FishDepth", 2 ) / 1000);
				FishWidth = (double) ((double)XmlAppSettings.Settings.GetSetting("FishWidth", 2 ) / 1000);
				TunnelCrossSection = (double) ((double)XmlAppSettings.Settings.GetSetting("TunnelCrossSection", 2 ) / 1000);
				daqUpdateTimer.Interval = DAQ_UPDATE_DELAY;
				daqUpdateTimer.Tick += DaqUpdatetimerTick;
				daqUpdateTimer.Enabled = true;
			}
			catch(Exception)
			{
				
			}
		}
		
		public enum FlowScaleModeEnum { FL, ms, V };
		
		public Respirometer.RespMotorControl.FlowScaleModeEnum flowScaleMode = FlowScaleModeEnum.FL;
		public FlowScaleModeEnum FlowScaleMode {
			get { return flowScaleMode; }
			set { flowScaleMode = value;}
		}

		const double calibrationTablePrecision = 0.01;
		const int calibrationTableSize = (int)(maxVoltageSetting / calibrationTablePrecision) + 1;
		double[] calibrationTable = new double[calibrationTableSize];
		void MakeCalibrationLookUptable()
		{
			for(int i=0; i<calibrationTableSize; i++)
			{
				double V = i * calibrationTablePrecision;
				calibrationTable[i] = ( V * V * CalibrationFactor2) +  (V * CalibrationFactor1) + CalibrationFactor0;
			}
		}
		
		double VoltsToFlow( double volts )
		{
			int i = (int)(volts / calibrationTablePrecision);
			return calibrationTable[i];
			//return ( volts * CalibrationFactor1 * CalibrationFactor1) +  (volts * CalibrationFactor1) + CalibrationFactor0;
		}

		double FlowToVolts( double flow )
		{
			for(int i=0; i<calibrationTableSize; i++)
			{
				if( calibrationTable[i] > flow )
				{
					return i * calibrationTablePrecision;
				}
			}
			return  maxVoltageSetting;
		}
		
		double SelectedUnitsToVolts( double flow ) {
			switch(flowScaleMode){
					case FlowScaleModeEnum.FL: return FlowToVolts(flow * FishLength / SolidBlockFactor);
					case FlowScaleModeEnum.ms: return FlowToVolts(flow);
					case FlowScaleModeEnum.V: return flow;
					default : return 0;
			}
		}
		
		double VoltsToSelectedUnits( double volts ) {
			switch(flowScaleMode){
					case FlowScaleModeEnum.FL: return VoltsToFlow(volts)/FishLength*SolidBlockFactor;
					case FlowScaleModeEnum.ms: return VoltsToFlow(volts);
					case FlowScaleModeEnum.V: return volts;
					default : return 0;
			}
		}
		
		double SolidBlockFactor {
			get {
				if(checkBoxSolidBlockCompEnable.Checked) {
					double fishRadius = (FishWidth + fishDepth)/4;
					double fishArea = System.Math.PI * fishRadius * fishRadius;
					return 1 + (0.4 * (FishLength/fishRadius) * System.Math.Pow( (fishArea/tunnelCrossSection), 1.5 ));
				} else return 1.0;
			}
		}
		
		
		public bool UsingLength {
			get {
				return FlowScaleMode == FlowScaleModeEnum.FL;
			}
		}

		public int fsm {
			get {
				return (int)FlowScaleMode;
			}
			set {
				FlowScaleMode = (FlowScaleModeEnum)value;
			}
		}
		
		public void InitUI()
		{
			comboBox1.SelectedIndex = 2;
			comboBox1.DataBindings.Add("SelectedIndex", this, "fsm", false, DataSourceUpdateMode.OnPropertyChanged);
			textBoxCalibrationFactor0.DataBindings.Add("Text", this, "CalibrationFactor0");
			textBoxCalibrationFactor1.DataBindings.Add("Text", this, "CalibrationFactor1");
			textBoxCalibrationFactor2.DataBindings.Add("Text", this, "CalibrationFactor2");
			textBoxTunnelCrossSection.DataBindings.Add("Enabled", checkBoxSolidBlockCompEnable , "Checked");
			textBoxFishDepth.DataBindings.Add("Enabled", checkBoxSolidBlockCompEnable, "Checked");
			textBoxFishWidth.DataBindings.Add("Enabled", checkBoxSolidBlockCompEnable, "Checked");
			textBoxFishLength.DataBindings.Add("Enabled", this, "UsingLength");
			groupBoxSolidBlocking.DataBindings.Add("Enabled", this, "UsingLength");
			textBoxFishLength.DataBindings.Add("Text", this, "FishLength");
			textBoxFishDepth.DataBindings.Add("Text", this, "FishDepth");
			textBoxFishWidth.DataBindings.Add("Text", this, "FishWidth");
			textBoxTunnelCrossSection.DataBindings.Add("Text", this, "TunnelCrossSection");
			
			trackBarSetSpeed.Maximum =  (int)(maxVoltageSetting * 100);
			trackBarSetSpeed.DataBindings.Add("Value", this, "SetPointx100", false, DataSourceUpdateMode.OnPropertyChanged);
			textBoxSetSpeed.DataBindings.Add("Text", this, "SetPointinSelectedUnits", false, DataSourceUpdateMode.OnValidation);
			
			//textBoxSetSpeed.DataBindings.Add("Text", this, "SetPointinSelectedUnits", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "#0.00#");

			checkBoxEnableFlowControl.DataBindings.Add("Checked", this, "FlowControlEnabled", false, DataSourceUpdateMode.OnPropertyChanged);
		}
		
		private bool flowControlEnabled = true;
		public bool FlowControlEnabled {
			get { return flowControlEnabled; }
			set {
				flowControlEnabled = value;
				panel3.Enabled = value;
				XmlAppSettings.Settings.PutSetting("FlowControlEnabled", Convert.ToInt32( value ));
			}
		}
		
		private double calibrationFactor0 = 1;
		public double CalibrationFactor0 {
			set {
				calibrationFactor0 = value;
				XmlAppSettings.Settings.PutSetting("FlowCalibrationFactor0", (int)(calibrationFactor0*10000) );
				MakeCalibrationLookUptable();
			}
			get {
				return calibrationFactor0;
			}
		}
		
		private double calibrationFactor1 = 1;
		public double CalibrationFactor1 {
			set {
				calibrationFactor1 = value;
				XmlAppSettings.Settings.PutSetting("FlowCalibrationFactor1", (int)(calibrationFactor1*10000) );
				MakeCalibrationLookUptable();
			}
			get {
				return calibrationFactor1;
			}
		}
		
		private double calibrationFactor2 = 1;
		public double CalibrationFactor2 {
			set {
				calibrationFactor2 = value;
				XmlAppSettings.Settings.PutSetting("FlowCalibrationFactor2", (int)(calibrationFactor2*10000) );
				MakeCalibrationLookUptable();
			}
			get {
				return calibrationFactor2;
			}
		}

		
		double tunnelCrossSection;
		public double TunnelCrossSection {
			get { return tunnelCrossSection; }
			set {
				tunnelCrossSection = value;
				XmlAppSettings.Settings.PutSetting("TunnelCrossSection", (int)(value*1000) );
			}
		}

		private double fishLength = 0.02;
		public double FishLength {
			get { return fishLength; }
			set {
				fishLength = value;
				XmlAppSettings.Settings.PutSetting("FishLength", (int)(value*1000) );
			}
		}
		
		private double fishWidth = 0.02;
		public double FishWidth {
			get { return fishWidth; }
			set {
				fishWidth = value;
				XmlAppSettings.Settings.PutSetting("FishWidth", (int)(value*1000) );
			}
		}

		private double fishDepth = 0.02;
		
		public double FishDepth {
			get { return fishDepth; }
			set {
				fishDepth = value;
				XmlAppSettings.Settings.PutSetting("FishDepth", (int)(value*1000) );
			}
		}
		
		private double setPoint;
		public double SetPoint {
			get { return setPoint; }
			set {
				if(value > maxVoltageSetting) value = maxVoltageSetting;
				if(value < 0 ) value = 0;
				setPoint = System.Math.Round(  value , 2 );
				XmlAppSettings.Settings.PutSetting("FlowSetPoint", (int)(setPoint*100) );
				OnPropertyChangedEvent("    SetPoint");
			}
		}
		
		
		double lastSpeedSentToDaq = 0;
		void DaqUpdatetimerTick(object sender, EventArgs e)
		{
			if(setPoint != lastSpeedSentToDaq)
			{
				if( global.leighDaq is LeighDAQ && global.leighDaq.checkConnection())
				{
					(global.leighDaq as LeighDAQ).DaqWrite(1, setPoint );
				}
				lastSpeedSentToDaq = setPoint;
			}
		}
		
		public double SetPointx100 {
			get { return SetPoint * 100; }
			set { SetPoint = value/100; }
		}
		
		public double SetPointinSelectedUnits {
			get {
				return VoltsToSelectedUnits(setPoint);
			}
			set {
				SetPoint = SelectedUnitsToVolts(value);
				OnPropertyChangedEvent("SetPointinSelectedUnits");
			}
		}
		

		
		void Label1Click(object sender, EventArgs e)
		{
		}
		
		void TrackBarSetSpeedValidating(object sender, CancelEventArgs e)
		{
		}
		
		void Label1DoubleClick(object sender, EventArgs e)
		{
			textBoxCalibrationFactor0.Enabled = !textBoxCalibrationFactor0.Enabled;
			textBoxCalibrationFactor1.Enabled = textBoxCalibrationFactor0.Enabled;
			textBoxCalibrationFactor2.Enabled = textBoxCalibrationFactor0.Enabled;
		}
		
		void TrackBarSetSpeedMouseUp(object sender, MouseEventArgs e)
		{
		}
		
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}
		
		void CheckBoxSolidBlockCompEnableCheckedChanged(object sender, EventArgs e)
		{
			OnPropertyChangedEvent("SetPointinSelectedUnits");
		}
		
		public event PropertyChangedEventHandler PropertyChanged;
		private void OnPropertyChangedEvent(string propertyName)
		{
			if(PropertyChanged!=null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName) );
		}
		
		void TextBoxSetSpeedKeyPress(object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)Keys.Enter) {
				trackBarSetSpeed.Focus();
			}
		}
	}
}
