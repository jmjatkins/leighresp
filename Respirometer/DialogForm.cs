/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Respirometer
{
	/// <summary>
	/// Description of DialogForm.
	/// </summary>
	public partial class DialogForm : Form
	{
		public DialogForm()
		{
			InitializeComponent();
		}
		
		void DialogFormFormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = true;
			this.Controls.Clear();
			this.Hide();
		}
		
		static DialogForm singleton;
		static public void Show(Control control)
		{
			if (singleton==null) singleton = new DialogForm();
			singleton.Size = control.Size;
			singleton.Controls.Add(control);
			singleton.ShowDialog();
		}
	}
}
