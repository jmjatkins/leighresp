/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using LeighOxymeter;
using UnhandledExceptionHandler;

namespace Respirometer
{
	/// <summary>
	/// Description of global.
	/// </summary>
	public static class global
	{
		static public LeighDAQclient.LeighDaqInterface leighDaq;
		static public RespMotorControl respMotorControl;
		static public ExperiemntSetupControl experiemntSetupControl;
		static public RespPumpControl respPumpControl;
		static public RespMainPannelControl respMainPannelControl0;
		static public RespMainPannelControl respMainPannelControl1;
		static public RespMainPannelControl respMainPannelControl2;
		static public RespMainPannelControl respMainPannelControl3;
		static public TemperatureControl temperatureControl;
		static public StatusBanner statusBanner;

		
		
		static public void initialise()
		{
			UnhandledExceptionHandler.UnhandledExceptionHandler.Init("Leigh Resp");
			respPumpControl = new RespPumpControl();
			respMotorControl = new RespMotorControl();
			statusBanner = new StatusBanner();
			respMainPannelControl0 = new RespMainPannelControl();
			respMainPannelControl0.Channel = 0;
			respMainPannelControl1 = new RespMainPannelControl();
			respMainPannelControl1.Channel = 1;
			respMainPannelControl2 = new RespMainPannelControl();
			respMainPannelControl2.Channel = 2;
			respMainPannelControl3 = new RespMainPannelControl();
			respMainPannelControl3.Channel = 3;
			temperatureControl = new TemperatureControl();
			experiemntSetupControl = new ExperiemntSetupControl();
		}

	}
}
