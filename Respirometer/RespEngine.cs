/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.IO;
using System.Windows.Forms;
using LeighDAQclient;
using LeighOxymeter;
namespace Respirometer
{
	/// <summary>
	/// Description of RespEngine.
	/// </summary>
	public  class RespEngine : INotifyPropertyChanged
	{
		private void OnPropertyChangedEvent(string propertyName)
		{
			if(propertyChanged!=null)
				propertyChanged(this, new PropertyChangedEventArgs(propertyName) );
		}

		[NonSerialized]
		private PropertyChangedEventHandler propertyChanged;
		public event PropertyChangedEventHandler PropertyChanged {
			add {
				propertyChanged += value;
			}
			remove {
				propertyChanged -= value;
			}
		}
		
		static RespEngine instance;
		public static RespEngine Instance {
			get {
				if(instance==null) instance = new RespEngine();
				return instance;
			}
		}
		private bool initialisedAndReady = false;
		public  bool InitialisedAndReady {
			get { return initialisedAndReady; }
		}
		
		private System.Windows.Forms.Timer timer;
		
		
		public System.Windows.Forms.Control OxymeterUI {
			get {
				return LeighOxymeter.OxyMeterService.instance.Control;
			}
		}
		public void Initialise(  )
		{
			if(global.leighDaq is LeighDAQ)
			{
				(global.leighDaq as LeighDAQ).DaqReference = 10.0; //set reference voltage for DAC
			}
			LeighOxymeter.OxyMeterService.instance.OnRecievedO2measurement += OnGotNewOxymeterData;
			timer = new System.Windows.Forms.Timer();
			timer.Tick += timerTick;
			timer.Interval = 100;
			timer.Start();
			WaitPeriod =  XmlAppSettings.Settings.GetSetting("WaitPeriod", 240 );
			FlushPeriod =  XmlAppSettings.Settings.GetSetting("FlushPeriod", 60 );
			MeasurePeriod =  XmlAppSettings.Settings.GetSetting("MeasurePeriod", 300 );
			SpeedCyclesPerIncrement = XmlAppSettings.Settings.GetSetting("SpeedCyclesPerIncrement", 1 );
			SpeedDecrement = (double)XmlAppSettings.Settings.GetSetting("SpeedDecrement", 10 ) / 100;
			SpeedIncrement = (double)XmlAppSettings.Settings.GetSetting("SpeedIncrement", 10 ) / 100;
			MinSpeed = (double)XmlAppSettings.Settings.GetSetting("MinSpeed", 1000 ) / 100;
			MaxSpeed = (double)XmlAppSettings.Settings.GetSetting("MaxSpeed", 100 ) / 100;
			TemperatureControl.Instance.SetTemperature = (double)XmlAppSettings.Settings.GetSetting("TemperatureSetPoint", 200)/100;
			TemperatureControl.Instance.TempControlEnabled = XmlAppSettings.Settings.GetSetting("TemperatureControlEnabled", 0) == 1;
			TemperatureControl.Instance.HeatingMode= XmlAppSettings.Settings.GetSetting("TemperatureControlHeatingModeEnabled", 0) == 1;
			initialisedAndReady = true;
		}
		

		List<IOxymeterDataRecord> oxymeterDataRecords = new List<IOxymeterDataRecord>();
		private void OnGotNewOxymeterData(LeighOxymeter.IOxymeterDataRecord oxymeterDataRecord)
		{
			O2Reading = oxymeterDataRecord.O2;
			TemperatureReading = oxymeterDataRecord.Temperature;
			ControlTemperature();
			if(State == TState.Measure) {
				oxymeterDataRecords.Add(oxymeterDataRecord);
				LogMeasurement(oxymeterDataRecord);
			}
			OnLogMeasurement(oxymeterDataRecord, State == TState.Measure);
			if((State != TState.Stop) && !paused) {
			}
		}
		
		private void ControlTemperature()
		{
			if(TemperatureControl.Instance.TempControlEnabled) {
				bool pumpOn = TemperatureControl.Instance.HeatingMode ? 
					TemperatureReading < TemperatureControl.Instance.SetTemperature:
					TemperatureReading > TemperatureControl.Instance.SetTemperature;
					
				if(pumpOn != TemperatureControl.Instance.TempControlPumpOn)
				{
					TemperatureControl.Instance.TempControlPumpOn = pumpOn;
				}
			}
		}
		
		private List<double> o2Reading;
		public List<double> O2Reading {
			get { return o2Reading; }
			set { o2Reading = value;}
		}
		private double temperatureReading;
		public double TemperatureReading {
			get { return temperatureReading; }
			set { temperatureReading = value; OnPropertyChangedEvent("TemperatureReading");}
		}
		
		
		bool speedDecrementOnFlush;
		public bool SpeedDecrementOnFlush {
			get { return speedDecrementOnFlush; }
			set { speedDecrementOnFlush = value; }
		}

		bool speedIncrementEnable;
		public bool SpeedIncrementEnable {
			get { return speedIncrementEnable; }
			set { speedIncrementEnable = value; }
		}

		int speedCyclesPerIncrement;
		public int SpeedCyclesPerIncrement {
			get { return speedCyclesPerIncrement; }
			set {
				if(value < 1) value = 1;
				speedCyclesPerIncrement = value;
			}
		}
		
		int speedCycleIncrementCount;
		public int SpeedCycleIncrementCount {
			get { return speedCycleIncrementCount; }
			set { speedCycleIncrementCount = value; }
		}
		
		
		double speedDecrement;
		public double SpeedDecrement {
			get { return speedDecrement; }
			set { speedDecrement = value; }
		}

		double speedIncrement;
		public double SpeedIncrement {
			get { return speedIncrement; }
			set { speedIncrement = value; }
		}
		
		double maxSpeed;
		public double MaxSpeed {
			get { return maxSpeed; }
			set { maxSpeed = value; }
		}

		double minSpeed;
		public double MinSpeed {
			get { return minSpeed; }
			set { minSpeed = value; }
		}
		
		private void initialiseHardware()
		{
			try {
				//if(!LeighOxymeter.OxyMeterService.instance.IsReady) LeighOxymeter.OxyMeterService.instance.Initialise();
			}
			catch(Exception ex) {
				throw new Exception("Failed To Connect To Oxymeter. Got error:\n" + ex.Message);
			}
			if(global.leighDaq == null || ! global.leighDaq.checkConnection() ) 
				MessageBox.Show("No DAQ board detected", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				
				//throw new Exception("Failed To Connect To LeighDaq");
		}
		
		private bool hardwareIsReady {
			get {
				return  global.leighDaq!=null &&  global.leighDaq.checkConnection() && LeighOxymeter.OxyMeterService.instance.IsReady;
			}
		}
		
		public void SaveState()
		{
			XmlAppSettings.Settings.PutSetting("WaitPeriod", WaitPeriod );
			XmlAppSettings.Settings.PutSetting("FlushPeriod", FlushPeriod );
			XmlAppSettings.Settings.PutSetting("MeasurePeriod", MeasurePeriod );
			XmlAppSettings.Settings.PutSetting("SpeedIncrement", (int)(SpeedIncrement* 100) );
			XmlAppSettings.Settings.PutSetting("SpeedDecrement", (int)(SpeedDecrement* 100) );
			XmlAppSettings.Settings.PutSetting("MinSpeed", (int)(MinSpeed * 100) );
			XmlAppSettings.Settings.PutSetting("MaxSpeed", (int)(MaxSpeed * 100) );
			XmlAppSettings.Settings.PutSetting("SpeedCyclesPerIncrement", SpeedCyclesPerIncrement );
			XmlAppSettings.Settings.PutSetting("TemperatureSetPoint", (int)(TemperatureControl.Instance.SetTemperature * 100));
			XmlAppSettings.Settings.PutSetting("TemperatureControlEnabled", TemperatureControl.Instance.TempControlEnabled ? 1:0);
			XmlAppSettings.Settings.PutSetting("TemperatureControlHeatingModeEnabled", TemperatureControl.Instance.HeatingMode ? 1:0);

			
		}

		
		private bool paused;
		public bool Paused {
			get { return paused; }
			set {
				paused = value;
				OnPropertyChangedEvent("Paused");
			}
		}
		
		private void timerTick(object sender, EventArgs e)
		{
			timer.Enabled = false;
			try {
				if(!Paused)	{
					RunStateMachine();
				}
			}
			finally {
				timer.Enabled = true;
			}
		}
		
		public bool FlushPumpOn {
			set { RespPumpControl.Instance.FlushPumpOn = value;}
		}
		
		private int flushPeriod;
		public int FlushPeriod {
			get { return flushPeriod; }
			set { flushPeriod = value; OnPropertyChangedEvent("FlushPeriod");}
		}
		private int waitPeriod;
		public int WaitPeriod {
			get { return waitPeriod; }
			set { waitPeriod = value; OnPropertyChangedEvent("WaitPeriod");}
		}
		private int measurePeriod;
		public int MeasurePeriod {
			get { return measurePeriod; }
			set { measurePeriod = value; OnPropertyChangedEvent("MeasurePeriod");}
		}

		public TimeSpan TimeAlreadySpentInCurrentState {
			get{
				return DateTime.Now - enteredCurrentStateTime;
			}
		}

		public  enum TState { Stop, Start, PrepareToFlush, Flush, PrepareToWait, Wait, PrepareToMeasure, Measure, Process };
		private TState state;
		public  TState State {
			get { return state; }
			set {
				state = value;
				enteredCurrentStateTime = DateTime.Now;
				if(value == TState.Flush) CycleNumber++;
				StateString = State.ToString();
				if((value == TState.Flush)||(value == TState.Measure)||(value == TState.Wait)) {
					OnPhaseStart(DateTime.Now, State.ToString(), CycleNumber);
				}
			}
		}
		
		public int TimeRemainingInCurrentState {
			get {
				switch(state) {
						case TState.Stop: 		return 999;
						
						case TState.Start: 		return 0;
						case TState.PrepareToFlush: return 0;
						case TState.Flush: 		return FlushPeriod - (int)TimeAlreadySpentInCurrentState.TotalSeconds;
						case TState.PrepareToWait: return 0;
						case TState.Wait: 		return WaitPeriod - (int)TimeAlreadySpentInCurrentState.TotalSeconds;
						case TState.PrepareToMeasure: 	return 0;
						case TState.Measure: 	return MeasurePeriod - (int)TimeAlreadySpentInCurrentState.TotalSeconds;
						case TState.Process: 	return 0;
						default : return 0;
				}
			}
		}
		
		private DateTime enteredCurrentStateTime;
		private void UpdateState()
		{
			if( TimeRemainingInCurrentState <= 0 )
			{
				if(State < TState.Process)
					State++;
				else State = TState.PrepareToFlush;
			}
		}

		public void Start()
		{
			if(!hardwareIsReady) {
				try {
					initialiseHardware();
				}
				catch(Exception ex) {
					MessageBox.Show("Failed to inialise hardware. Got error:\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					return;
				}
			}
			
			if(State == TState.Stop)
			{
				CreateExperimentFolder();
				CycleNumber = 0;
				SpeedCycleIncrementCount = 0;
				OnExperiemntStart(DateTime.Now);
				State = TState.Wait;
			}
			Paused = false;
		}
		
		public void Reset()
		{
			State = TState.Stop;
			CycleNumber = 0;
		}

		private int cycleNumber;
		public int CycleNumber {
			get { return cycleNumber; }
			set {
				cycleNumber = value; OnPropertyChangedEvent("CycleNumber");
			}
		}
		
		private string stateString = "Stopped";
		public string StateString {
			get { return stateString; }
			set { stateString = value; OnPropertyChangedEvent("StateString");}
		}
		
		private string experimentFolderName;
		private void CreateExperimentFolder()
		{
			string dateString = String.Format("{0}-{1}-{2}-{3}-{4}-{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
			experimentFolderName = String.Format( "{0}\\{1}", System.Windows.Forms.Application.StartupPath, dateString);
			if(!System.IO.Directory.Exists( experimentFolderName))
			{
				System.IO.Directory.CreateDirectory(experimentFolderName);
			}
		}

		private TextWriter twLogMeasurement;
		private void CreateLogMeasurementFile()
		{
			if(twLogMeasurement !=null) twLogMeasurement.Close();
			string filename = String.Format("{0}\\Cycle{1}.csv",  experimentFolderName, CycleNumber);
			twLogMeasurement = new StreamWriter(new FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
			twLogMeasurement.WriteLine("Cycle file for cycle " + CycleNumber.ToString() +  " of respirometer experiement conducted on " + DateTime.Now.ToLongDateString().Replace(',', ':'));
			twLogMeasurement.WriteLine("Time,Temp(deg C),O2 Ch1,O2 Ch2,O2 Ch3,O2 Ch4");
			twLogMeasurement.WriteLine("");
		}
		
		private void LogMeasurement(IOxymeterDataRecord oxymeterDataRecord)
		{
			StringBuilder sb = new StringBuilder( String.Format("{0},{1}", oxymeterDataRecord.RecievedAt.ToLongTimeString(), oxymeterDataRecord.Temperature));
			foreach(double o2 in oxymeterDataRecord.O2)
				sb.Append(String.Format(",{0}", o2));
			twLogMeasurement.WriteLine(sb.ToString());
			twLogMeasurement.Flush();
		}

		private void ProcessMeasurements()
		{
			
			if(oxymeterDataRecords.Count < 2) return;
			
			int channels = 0;
			foreach(IOxymeterDataRecord or in oxymeterDataRecords) {
				if(or.O2.Count > channels) channels ++;
			}

			double y1;
			double y0;
			double e;
			double r;
			double[] x = new double[ oxymeterDataRecords.Count ];
			double[] y = new double[oxymeterDataRecords.Count];
			double[] yfit = new double[oxymeterDataRecords.Count];
			
			
			for( int c=0; c<channels; c++)
			{
				double temperature = 0;
				double avO2 = 0;

				for(int i=0; i<oxymeterDataRecords.Count; i++)
				{
					x[i] = oxymeterDataRecords[i].RecievedAt.Subtract(oxymeterDataRecords[0].RecievedAt).TotalSeconds ;
					y[i] = oxymeterDataRecords[i].O2[c];
					temperature += oxymeterDataRecords[i].Temperature;
					avO2 += oxymeterDataRecords[i].O2[c];
				}
				try {
					Math.Regression.LinearRegression( x, y, out y0, out y1, out yfit, out e, out r);
				}
				catch {
					y0 = 1;
					y1 = 0;
					r = 0;
				}
				temperature = temperature/(oxymeterDataRecords.Count);
				avO2 = avO2/(oxymeterDataRecords.Count);

				OnMeasurementsProcessed(c, oxymeterDataRecords[0].RecievedAt, y1, y0, r);
				string filename = String.Format("{0}\\Results -channel {1}.csv",  experimentFolderName, c);
				bool isNewFile = !System.IO.File.Exists(filename);
				TextWriter twLogResults = new StreamWriter(new FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
				if(isNewFile) {
					twLogResults.WriteLine("Results file for respirometer experiement conducted on " + DateTime.Now.ToLongDateString().Replace(',', ':'));
					twLogResults.WriteLine(String.Format("Channel {0}", c));
					twLogResults.WriteLine("");
					twLogResults.WriteLine("DateTime, dPO2/dt,R2,Temp(Deg C), Flow (" + global.respMotorControl.FlowScaleMode.ToString() + ")" +  ", mean PO2");
				}
				twLogResults.WriteLine(String.Format(  "{0},{1:0.0000},{2},{3},{4},{5}", DateTime.Now.ToLongTimeString(), y1, r, temperature, global.respMotorControl.SetPointinSelectedUnits, avO2  ));
				twLogResults.Flush();
				twLogResults.Close();
			}
			oxymeterDataRecords.Clear();
		}
		
		private void RunStateMachine()
		{
			UpdateState();
			OnPropertyChangedEvent("TimeRemainingInCurrentState");
			switch(State) {
				case TState.Stop:
					break;

				case TState.Start:
					break;
					
				case TState.PrepareToFlush:
					if(SpeedDecrementOnFlush){
						double newSpeed = global.respMotorControl.SetPointinSelectedUnits - speedDecrement;
						newSpeed = System.Math.Min(newSpeed, maxSpeed);
						newSpeed = System.Math.Max(newSpeed, minSpeed);
						global.respMotorControl.SetPointinSelectedUnits = newSpeed;
					}
					break;
					
				case TState.Flush:
					FlushPumpOn = true;
					break;
					
				case TState.PrepareToWait:
					if(SpeedDecrementOnFlush){
						double newSpeed = global.respMotorControl.SetPointinSelectedUnits + speedDecrement;
						newSpeed = System.Math.Min(newSpeed, maxSpeed);
						newSpeed = System.Math.Max(newSpeed, minSpeed);
						global.respMotorControl.SetPointinSelectedUnits = newSpeed;
					}
					if(SpeedIncrementEnable){
						if(++SpeedCycleIncrementCount >= speedCyclesPerIncrement)
						{
							SpeedCycleIncrementCount = 0;
							double newSpeed = global.respMotorControl.SetPointinSelectedUnits + speedIncrement;
							newSpeed = System.Math.Min(newSpeed, maxSpeed);
							newSpeed = System.Math.Max(newSpeed, minSpeed);
							global.respMotorControl.SetPointinSelectedUnits = newSpeed;
						}
					}
					break;
				case TState.Wait:
					FlushPumpOn = false;
					break;
					
				case TState.PrepareToMeasure:
					CreateLogMeasurementFile();
					break;

				case TState.Measure:
					FlushPumpOn = false;
					break;

				case TState.Process:
					ProcessMeasurements();
					break;
			}
		}
		

		
		public delegate void ExperiemntStartEventHandler(DateTime startTime);
		private ExperiemntStartEventHandler experiemntStartEvent;
		public event ExperiemntStartEventHandler ExperiemntStartEvent {
			add {experiemntStartEvent+=value;}
			remove {experiemntStartEvent-=value;}
		}
		private void OnExperiemntStart(DateTime startTime)
		{
			if(experiemntStartEvent!=null) experiemntStartEvent(startTime);
		}

		public delegate void PhaseStartEventHandler(DateTime timeStamp, string phaseName, int cycleNumber);
		private PhaseStartEventHandler phaseStartEvent;
		public event PhaseStartEventHandler PhaseStartEvent {
			add {phaseStartEvent+=value;}
			remove {phaseStartEvent-=value;}
		}
		private void OnPhaseStart(DateTime timeStamp, string phaseName, int cycleNumber)
		{
			if(phaseStartEvent!=null) phaseStartEvent(timeStamp, phaseName, cycleNumber);
		}
		
		public delegate void LogMeasurementEventHandler(LeighOxymeter.IOxymeterDataRecord oxymeterDataRecord, bool logged);
		private LogMeasurementEventHandler logO2Event;
		public event LogMeasurementEventHandler GotO2Event {
			add {logO2Event+=value;}
			remove {logO2Event-=value;}
		}
		private void OnLogMeasurement(LeighOxymeter.IOxymeterDataRecord oxymeterDataRecord, bool logged)
		{
			if(logO2Event!=null) logO2Event(oxymeterDataRecord, logged);
		}
		
		public delegate void MeasurementsProcessedEventHandler(int channel, DateTime timeStamp, double y1, double y0, double r);
		private MeasurementsProcessedEventHandler measurementsProcessedEvent;
		public event MeasurementsProcessedEventHandler MeasurementsProcessedEvent {
			add {measurementsProcessedEvent+=value;}
			remove {measurementsProcessedEvent-=value;}
		}
		private void OnMeasurementsProcessed(int channel, DateTime timeStamp, double y1, double y0, double r)
		{
			if(measurementsProcessedEvent!=null) measurementsProcessedEvent(channel, timeStamp, y1, y0, r);
		}
	}

}