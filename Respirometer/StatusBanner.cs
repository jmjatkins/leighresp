/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Respirometer
{
	/// <summary>
	/// Description of StatusBanner.
	/// </summary>
	public partial class StatusBanner : DockContent
	{
		RespEngine respEngine;
		public StatusBanner()
		{
			InitializeComponent();
			HideOnClose = true;
			TabText = "Status";
		}
		
		void StatusBannerLoad(object sender, EventArgs e)
		{
			respEngine = RespEngine.Instance;
			label5.DataBindings.Add("Text", respEngine ,"StateString");
		}
	}
}
