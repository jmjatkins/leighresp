/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Respirometer
{
	partial class ExperiemntSetupControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControlSpeedControl = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.button7 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textBoxSpeedIncCycleCount = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.textBoxSpeedMin = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.textBoxSpeedMax = new System.Windows.Forms.TextBox();
			this.label1SpeedDec = new System.Windows.Forms.Label();
			this.textBoxSpeedDec = new System.Windows.Forms.TextBox();
			this.labelSpeedInc = new System.Windows.Forms.Label();
			this.textBoxSpeedInc = new System.Windows.Forms.TextBox();
			this.checkBoxSpeedDecFlush = new System.Windows.Forms.CheckBox();
			this.checkBoxSpeedIncEachCyc = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.tabControlSpeedControl.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControlSpeedControl
			// 
			this.tabControlSpeedControl.Controls.Add(this.tabPage2);
			this.tabControlSpeedControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControlSpeedControl.Location = new System.Drawing.Point(0, 0);
			this.tabControlSpeedControl.Name = "tabControlSpeedControl";
			this.tabControlSpeedControl.SelectedIndex = 0;
			this.tabControlSpeedControl.Size = new System.Drawing.Size(722, 215);
			this.tabControlSpeedControl.TabIndex = 3;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.groupBox2);
			this.tabPage2.Controls.Add(this.groupBox1);
			this.tabPage2.Controls.Add(this.label1);
			this.tabPage2.Controls.Add(this.textBox3);
			this.tabPage2.Controls.Add(this.textBox2);
			this.tabPage2.Controls.Add(this.textBox1);
			this.tabPage2.Controls.Add(this.label2);
			this.tabPage2.Controls.Add(this.label3);
			this.tabPage2.Controls.Add(this.label4);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(714, 189);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Cycle";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.button7);
			this.groupBox2.Controls.Add(this.button5);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
			this.groupBox2.Location = new System.Drawing.Point(540, 3);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(10);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(10);
			this.groupBox2.Size = new System.Drawing.Size(171, 183);
			this.groupBox2.TabIndex = 10;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Run Control";
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(16, 91);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(96, 42);
			this.button7.TabIndex = 2;
			this.button7.Text = "Stop";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.Button7Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(16, 40);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(96, 45);
			this.button5.TabIndex = 0;
			this.button5.Text = "Run";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Button5Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textBoxSpeedIncCycleCount);
			this.groupBox1.Controls.Add(this.label20);
			this.groupBox1.Controls.Add(this.label19);
			this.groupBox1.Controls.Add(this.textBoxSpeedMin);
			this.groupBox1.Controls.Add(this.label18);
			this.groupBox1.Controls.Add(this.textBoxSpeedMax);
			this.groupBox1.Controls.Add(this.label1SpeedDec);
			this.groupBox1.Controls.Add(this.textBoxSpeedDec);
			this.groupBox1.Controls.Add(this.labelSpeedInc);
			this.groupBox1.Controls.Add(this.textBoxSpeedInc);
			this.groupBox1.Controls.Add(this.checkBoxSpeedDecFlush);
			this.groupBox1.Controls.Add(this.checkBoxSpeedIncEachCyc);
			this.groupBox1.Location = new System.Drawing.Point(161, 9);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(319, 138);
			this.groupBox1.TabIndex = 9;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Speed Control";
			// 
			// textBoxSpeedIncCycleCount
			// 
			this.textBoxSpeedIncCycleCount.Location = new System.Drawing.Point(104, 20);
			this.textBoxSpeedIncCycleCount.Name = "textBoxSpeedIncCycleCount";
			this.textBoxSpeedIncCycleCount.Size = new System.Drawing.Size(36, 20);
			this.textBoxSpeedIncCycleCount.TabIndex = 18;
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(146, 23);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(44, 15);
			this.label20.TabIndex = 17;
			this.label20.Text = "Cycles";
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(215, 105);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(39, 15);
			this.label19.TabIndex = 16;
			this.label19.Text = "Min";
			// 
			// textBoxSpeedMin
			// 
			this.textBoxSpeedMin.Location = new System.Drawing.Point(260, 102);
			this.textBoxSpeedMin.Name = "textBoxSpeedMin";
			this.textBoxSpeedMin.Size = new System.Drawing.Size(56, 20);
			this.textBoxSpeedMin.TabIndex = 15;
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(215, 52);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(39, 15);
			this.label18.TabIndex = 14;
			this.label18.Text = "Max";
			// 
			// textBoxSpeedMax
			// 
			this.textBoxSpeedMax.Location = new System.Drawing.Point(260, 49);
			this.textBoxSpeedMax.Name = "textBoxSpeedMax";
			this.textBoxSpeedMax.Size = new System.Drawing.Size(56, 20);
			this.textBoxSpeedMax.TabIndex = 13;
			// 
			// label1SpeedDec
			// 
			this.label1SpeedDec.Location = new System.Drawing.Point(26, 107);
			this.label1SpeedDec.Name = "label1SpeedDec";
			this.label1SpeedDec.Size = new System.Drawing.Size(82, 15);
			this.label1SpeedDec.TabIndex = 12;
			this.label1SpeedDec.Text = "Speed Dec";
			// 
			// textBoxSpeedDec
			// 
			this.textBoxSpeedDec.Location = new System.Drawing.Point(114, 104);
			this.textBoxSpeedDec.Name = "textBoxSpeedDec";
			this.textBoxSpeedDec.Size = new System.Drawing.Size(56, 20);
			this.textBoxSpeedDec.TabIndex = 11;
			// 
			// labelSpeedInc
			// 
			this.labelSpeedInc.Location = new System.Drawing.Point(26, 52);
			this.labelSpeedInc.Name = "labelSpeedInc";
			this.labelSpeedInc.Size = new System.Drawing.Size(82, 15);
			this.labelSpeedInc.TabIndex = 10;
			this.labelSpeedInc.Text = "Speed Inc";
			// 
			// textBoxSpeedInc
			// 
			this.textBoxSpeedInc.Location = new System.Drawing.Point(114, 49);
			this.textBoxSpeedInc.Name = "textBoxSpeedInc";
			this.textBoxSpeedInc.Size = new System.Drawing.Size(56, 20);
			this.textBoxSpeedInc.TabIndex = 2;
			// 
			// checkBoxSpeedDecFlush
			// 
			this.checkBoxSpeedDecFlush.Location = new System.Drawing.Point(6, 80);
			this.checkBoxSpeedDecFlush.Name = "checkBoxSpeedDecFlush";
			this.checkBoxSpeedDecFlush.Size = new System.Drawing.Size(134, 24);
			this.checkBoxSpeedDecFlush.TabIndex = 1;
			this.checkBoxSpeedDecFlush.Text = "Reduce For Flush";
			this.checkBoxSpeedDecFlush.UseVisualStyleBackColor = true;
			// 
			// checkBoxSpeedIncEachCyc
			// 
			this.checkBoxSpeedIncEachCyc.Location = new System.Drawing.Point(6, 19);
			this.checkBoxSpeedIncEachCyc.Name = "checkBoxSpeedIncEachCyc";
			this.checkBoxSpeedIncEachCyc.Size = new System.Drawing.Size(154, 24);
			this.checkBoxSpeedIncEachCyc.TabIndex = 0;
			this.checkBoxSpeedIncEachCyc.Text = "Increase Each";
			this.checkBoxSpeedIncEachCyc.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(123, 23);
			this.label1.TabIndex = 8;
			this.label1.Text = "Cycle Periods (s):";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(68, 91);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(66, 20);
			this.textBox3.TabIndex = 7;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(68, 65);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(66, 20);
			this.textBox2.TabIndex = 6;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(68, 39);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(66, 20);
			this.textBox1.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(43, 15);
			this.label2.TabIndex = 2;
			this.label2.Text = "Flush";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(43, 15);
			this.label3.TabIndex = 3;
			this.label3.Text = "Wait";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 94);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(54, 15);
			this.label4.TabIndex = 4;
			this.label4.Text = "Measure";
			// 
			// ExperiemntSetupControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(722, 215);
			this.Controls.Add(this.tabControlSpeedControl);
			this.Name = "ExperiemntSetupControl";
			this.Load += new System.EventHandler(this.ExperiemntSetupControlLoad);
			this.tabControlSpeedControl.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox checkBoxSpeedIncEachCyc;
		private System.Windows.Forms.CheckBox checkBoxSpeedDecFlush;
		private System.Windows.Forms.TextBox textBoxSpeedInc;
		private System.Windows.Forms.Label labelSpeedInc;
		private System.Windows.Forms.TextBox textBoxSpeedDec;
		private System.Windows.Forms.Label label1SpeedDec;
		private System.Windows.Forms.TextBox textBoxSpeedMax;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox textBoxSpeedMin;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox textBoxSpeedIncCycleCount;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabControl tabControlSpeedControl;
	}
}
