/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using LeighDAQclient;
//using MicroxOxyMeter;
using XmlAppSettings;
using WeifenLuo.WinFormsUI.Docking;

namespace Respirometer
{
	/// <summary>
	/// Description of RespMainPannelControl.
	/// </summary>
	public partial class RespMainPannelControl : DockContent
	{
		//XmlAppSettings.Settings settings = new XmlAppSettings.Settings();
		RespEngine respEngine;
		public RespMainPannelControl()
		{
			InitializeComponent();
			HideOnClose = true;
		}
		
		void RespMainPannelControlLoad(object sender, EventArgs e)
		{
			TabText = String.Format("Channel {0}", Channel);
			respEngine = RespEngine.Instance;
			label11.DataBindings.Add("Text", respEngine ,"CycleNumber");
			label14.DataBindings.Add("Text", respEngine ,"TimeRemainingInCurrentState");
			//label7.DataBindings.Add("Text", respEngine ,"O2ReadingChan0");
			
			//label20.DataBindings.Add("Text", global.oxyMeter ,"O2UnitString");

			label9.DataBindings.Add("Text", respEngine ,"TemperatureReading");
			
			respEngine.GotO2Event += GotO2reading;
			respEngine.MeasurementsProcessedEvent += OnMeasurementProcessed;
			respEngine.ExperiemntStartEvent += OnExperiementStart;
			respEngine.PhaseStartEvent += OnPhaseStart;
		}
		
		protected override string GetPersistString()
		{
			return String.Format("RespMainPannelControl{0}", Channel.ToString());
		}
		
		
		private int channel;
		public int Channel {
			get { return channel; }
			set {
				channel = value;
				TabText = String.Format("Channel {0}", Channel);
			}
		}
		
		private DateTime experimentStartTime = DateTime.Now;
		private void OnExperiementStart(DateTime startTime)
		{
			experimentStartTime = DateTime.Now;
			for(int i=0; i<tChart1.Series.Count; i++)
				tChart1.Series[i].Clear();
		}
		
		private void OnPhaseStart(DateTime startTime, string phaseName, int cycleNumber)
		{
			if(tChart1.Series[0].Count > 3600/2 ) {//clear plot every half hour
				for(int i=0; i<tChart1.Series.Count; i++) tChart1.Series[i].Clear();
			}
			tChart1.Series[0].Add(startTime.Subtract( experimentStartTime).TotalSeconds, 50, String.Format("{0}->", phaseName,cycleNumber));
		}

		Random r = new Random(8);
		bool  lastValueWasLogged = false;
		void GotO2reading(LeighOxymeter.IOxymeterDataRecord oxymeterDataRecord, bool logged)
		{
			label7.Text = oxymeterDataRecord.O2[channel].ToString("F2");
			if((respEngine.State != RespEngine.TState.Stop) && !respEngine.Paused) {
				TimeSpan elapsedTime = oxymeterDataRecord.RecievedAt - experimentStartTime;
				tChart1.Series[1].Add(elapsedTime.TotalSeconds, oxymeterDataRecord.O2[channel], (logged && lastValueWasLogged)? Color.Blue:Color.Yellow);
				lastValueWasLogged = logged;
				tChart1.Axes.Bottom.Maximum = elapsedTime.TotalSeconds;
				tChart1.Axes.Bottom.Minimum = elapsedTime.TotalSeconds - ( (respEngine.MeasurePeriod + respEngine.WaitPeriod + respEngine.FlushPeriod) * 2);
				tChart1.Axes.Top.Maximum = elapsedTime.TotalSeconds;
				tChart1.Axes.Top.Minimum = elapsedTime.TotalSeconds - ((respEngine.MeasurePeriod + respEngine.WaitPeriod + respEngine.FlushPeriod) * 2) ;
			}
		}
		
		
		void OnMeasurementProcessed(int channel, DateTime timeStamp, double y1, double y0, double r)
		{
			if(channel != this.channel) return;
			TimeSpan elapsedTime = timeStamp - experimentStartTime;
			tChart1.Series[2].Clear();
			tChart1.Series[2].Add(elapsedTime.TotalSeconds, y0);
			tChart1.Series[2].Add(elapsedTime.TotalSeconds + respEngine.MeasurePeriod, y0 + ( y1 * respEngine.MeasurePeriod));
			label15.Text = (y1*-1.0).ToString("F2");
			label17.Text = r.ToString("F2");
		}
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			try {
			}
			catch {}
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		
		void Timer1Tick(object sender, EventArgs e)
		{
			//StateMachine();
		}
		
		
		void Button6Click(object sender, EventArgs e)
		{
			
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			//System.Diagnostics.Process.Start("Explorer", String.Format( "\"{0}\"", BaseDirectory) );
		}
		
		
		void Panel3Paint(object sender, PaintEventArgs e)
		{
			
		}
		
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
		}
		
		void TChart1Click(object sender, EventArgs e)
		{
			
		}
		
		void RespMainPannelControlFormClosing(object sender, FormClosingEventArgs e)
		{
		}
	}
}
