/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Respirometer
{
	partial class RespMotorControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.trackBarSetSpeed = new System.Windows.Forms.TrackBar();
			this.textBoxSetSpeed = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.groupBoxSolidBlocking = new System.Windows.Forms.GroupBox();
			this.checkBoxSolidBlockCompEnable = new System.Windows.Forms.CheckBox();
			this.textBoxFishDepth = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxFishWidth = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxTunnelCrossSection = new System.Windows.Forms.TextBox();
			this.textBoxFishLength = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textBoxCalibrationFactor0 = new System.Windows.Forms.TextBox();
			this.textBoxCalibrationFactor1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.checkBoxEnableFlowControl = new System.Windows.Forms.CheckBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.textBoxCalibrationFactor2 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarSetSpeed)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.groupBoxSolidBlocking.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.comboBox1);
			this.groupBox4.Controls.Add(this.trackBarSetSpeed);
			this.groupBox4.Controls.Add(this.textBoxSetSpeed);
			this.groupBox4.Location = new System.Drawing.Point(3, 6);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(295, 138);
			this.groupBox4.TabIndex = 6;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Flow Velocity";
			// 
			// comboBox1
			// 
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
									"BL/s",
									"m/s",
									"V"});
			this.comboBox1.Location = new System.Drawing.Point(147, 39);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(90, 28);
			this.comboBox1.TabIndex = 6;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// trackBarSetSpeed
			// 
			this.trackBarSetSpeed.Location = new System.Drawing.Point(6, 87);
			this.trackBarSetSpeed.Maximum = 1000;
			this.trackBarSetSpeed.Name = "trackBarSetSpeed";
			this.trackBarSetSpeed.Size = new System.Drawing.Size(267, 45);
			this.trackBarSetSpeed.TabIndex = 5;
			this.trackBarSetSpeed.Value = 1;
			this.trackBarSetSpeed.Validating += new System.ComponentModel.CancelEventHandler(this.TrackBarSetSpeedValidating);
			this.trackBarSetSpeed.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrackBarSetSpeedMouseUp);
			// 
			// textBoxSetSpeed
			// 
			this.textBoxSetSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxSetSpeed.Location = new System.Drawing.Point(28, 38);
			this.textBoxSetSpeed.Name = "textBoxSetSpeed";
			this.textBoxSetSpeed.Size = new System.Drawing.Size(100, 29);
			this.textBoxSetSpeed.TabIndex = 4;
			this.textBoxSetSpeed.Text = "0.00";
			this.textBoxSetSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textBoxSetSpeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxSetSpeedKeyPress);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.groupBoxSolidBlocking);
			this.groupBox3.Controls.Add(this.textBoxFishLength);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Location = new System.Drawing.Point(3, 150);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(295, 220);
			this.groupBox3.TabIndex = 5;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Setup Parameters";
			// 
			// groupBoxSolidBlocking
			// 
			this.groupBoxSolidBlocking.Controls.Add(this.checkBoxSolidBlockCompEnable);
			this.groupBoxSolidBlocking.Controls.Add(this.textBoxFishDepth);
			this.groupBoxSolidBlocking.Controls.Add(this.label3);
			this.groupBoxSolidBlocking.Controls.Add(this.label4);
			this.groupBoxSolidBlocking.Controls.Add(this.textBoxFishWidth);
			this.groupBoxSolidBlocking.Controls.Add(this.label6);
			this.groupBoxSolidBlocking.Controls.Add(this.textBoxTunnelCrossSection);
			this.groupBoxSolidBlocking.Location = new System.Drawing.Point(6, 48);
			this.groupBoxSolidBlocking.Name = "groupBoxSolidBlocking";
			this.groupBoxSolidBlocking.Size = new System.Drawing.Size(279, 132);
			this.groupBoxSolidBlocking.TabIndex = 9;
			this.groupBoxSolidBlocking.TabStop = false;
			this.groupBoxSolidBlocking.Text = "Solid Blocking";
			// 
			// checkBoxSolidBlockCompEnable
			// 
			this.checkBoxSolidBlockCompEnable.Location = new System.Drawing.Point(6, 19);
			this.checkBoxSolidBlockCompEnable.Name = "checkBoxSolidBlockCompEnable";
			this.checkBoxSolidBlockCompEnable.Size = new System.Drawing.Size(212, 24);
			this.checkBoxSolidBlockCompEnable.TabIndex = 0;
			this.checkBoxSolidBlockCompEnable.Text = "Copensate For Solid Blocking Effect";
			this.checkBoxSolidBlockCompEnable.UseVisualStyleBackColor = true;
			this.checkBoxSolidBlockCompEnable.CheckedChanged += new System.EventHandler(this.CheckBoxSolidBlockCompEnableCheckedChanged);
			// 
			// textBoxFishDepth
			// 
			this.textBoxFishDepth.Location = new System.Drawing.Point(190, 101);
			this.textBoxFishDepth.Name = "textBoxFishDepth";
			this.textBoxFishDepth.Size = new System.Drawing.Size(81, 20);
			this.textBoxFishDepth.TabIndex = 8;
			this.textBoxFishDepth.Text = "0.0";
			this.textBoxFishDepth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(22, 52);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(158, 23);
			this.label3.TabIndex = 1;
			this.label3.Text = "Tunnel Cross Section (m2):";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(22, 78);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 2;
			this.label4.Text = "Fish Width (m):";
			// 
			// textBoxFishWidth
			// 
			this.textBoxFishWidth.Location = new System.Drawing.Point(190, 75);
			this.textBoxFishWidth.Name = "textBoxFishWidth";
			this.textBoxFishWidth.Size = new System.Drawing.Size(81, 20);
			this.textBoxFishWidth.TabIndex = 6;
			this.textBoxFishWidth.Text = "0.0";
			this.textBoxFishWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(22, 104);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 23);
			this.label6.TabIndex = 4;
			this.label6.Text = "Fish Depth (m):";
			// 
			// textBoxTunnelCrossSection
			// 
			this.textBoxTunnelCrossSection.Location = new System.Drawing.Point(190, 49);
			this.textBoxTunnelCrossSection.Name = "textBoxTunnelCrossSection";
			this.textBoxTunnelCrossSection.Size = new System.Drawing.Size(81, 20);
			this.textBoxTunnelCrossSection.TabIndex = 5;
			this.textBoxTunnelCrossSection.Text = "0.0";
			this.textBoxTunnelCrossSection.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxFishLength
			// 
			this.textBoxFishLength.Location = new System.Drawing.Point(196, 19);
			this.textBoxFishLength.Name = "textBoxFishLength";
			this.textBoxFishLength.Size = new System.Drawing.Size(81, 20);
			this.textBoxFishLength.TabIndex = 7;
			this.textBoxFishLength.Text = "10.0";
			this.textBoxFishLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(12, 22);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 23);
			this.label5.TabIndex = 3;
			this.label5.Text = "Fish Length (m):";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.textBoxCalibrationFactor2);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.textBoxCalibrationFactor0);
			this.groupBox1.Controls.Add(this.textBoxCalibrationFactor1);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(3, 376);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(295, 102);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Calibration";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(148, 80);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(20, 17);
			this.label8.TabIndex = 15;
			this.label8.Text = "a0";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(147, 54);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(24, 17);
			this.label7.TabIndex = 14;
			this.label7.Text = "a1";
			// 
			// textBoxCalibrationFactor0
			// 
			this.textBoxCalibrationFactor0.Enabled = false;
			this.textBoxCalibrationFactor0.Location = new System.Drawing.Point(177, 77);
			this.textBoxCalibrationFactor0.Name = "textBoxCalibrationFactor0";
			this.textBoxCalibrationFactor0.Size = new System.Drawing.Size(100, 20);
			this.textBoxCalibrationFactor0.TabIndex = 12;
			this.textBoxCalibrationFactor0.Text = "0.0";
			this.textBoxCalibrationFactor0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxCalibrationFactor1
			// 
			this.textBoxCalibrationFactor1.Enabled = false;
			this.textBoxCalibrationFactor1.Location = new System.Drawing.Point(177, 51);
			this.textBoxCalibrationFactor1.Name = "textBoxCalibrationFactor1";
			this.textBoxCalibrationFactor1.Size = new System.Drawing.Size(100, 20);
			this.textBoxCalibrationFactor1.TabIndex = 11;
			this.textBoxCalibrationFactor1.Text = "0.0";
			this.textBoxCalibrationFactor1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 29);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(90, 23);
			this.label1.TabIndex = 9;
			this.label1.Text = "Calibration";
			this.label1.DoubleClick += new System.EventHandler(this.Label1DoubleClick);
			this.label1.Click += new System.EventHandler(this.Label1Click);
			// 
			// checkBoxEnableFlowControl
			// 
			this.checkBoxEnableFlowControl.Location = new System.Drawing.Point(15, 3);
			this.checkBoxEnableFlowControl.Name = "checkBoxEnableFlowControl";
			this.checkBoxEnableFlowControl.Size = new System.Drawing.Size(146, 24);
			this.checkBoxEnableFlowControl.TabIndex = 9;
			this.checkBoxEnableFlowControl.Text = "Enable Flow Control";
			this.checkBoxEnableFlowControl.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.checkBoxEnableFlowControl);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(306, 31);
			this.panel2.TabIndex = 10;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.groupBox1);
			this.panel3.Controls.Add(this.groupBox4);
			this.panel3.Controls.Add(this.groupBox3);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 31);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(306, 492);
			this.panel3.TabIndex = 11;
			// 
			// textBoxCalibrationFactor2
			// 
			this.textBoxCalibrationFactor2.Enabled = false;
			this.textBoxCalibrationFactor2.Location = new System.Drawing.Point(177, 25);
			this.textBoxCalibrationFactor2.Name = "textBoxCalibrationFactor2";
			this.textBoxCalibrationFactor2.Size = new System.Drawing.Size(100, 20);
			this.textBoxCalibrationFactor2.TabIndex = 16;
			this.textBoxCalibrationFactor2.Text = "0.0";
			this.textBoxCalibrationFactor2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(147, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 17);
			this.label2.TabIndex = 17;
			this.label2.Text = "a2";
			// 
			// RespMotorControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Name = "RespMotorControl";
			this.Size = new System.Drawing.Size(306, 523);
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarSetSpeed)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBoxSolidBlocking.ResumeLayout(false);
			this.groupBoxSolidBlocking.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.TextBox textBoxCalibrationFactor2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox checkBoxSolidBlockCompEnable;
		private System.Windows.Forms.TextBox textBoxCalibrationFactor1;
		private System.Windows.Forms.TextBox textBoxCalibrationFactor0;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.CheckBox checkBoxEnableFlowControl;
		private System.Windows.Forms.GroupBox groupBoxSolidBlocking;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.TextBox textBoxTunnelCrossSection;
		private System.Windows.Forms.TrackBar trackBarSetSpeed;
		private System.Windows.Forms.TextBox textBoxSetSpeed;
		private System.Windows.Forms.TextBox textBoxFishDepth;
		private System.Windows.Forms.TextBox textBoxFishLength;
		private System.Windows.Forms.TextBox textBoxFishWidth;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label3;
	}
}
