/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Respirometer
{
	/// <summary>
	/// Description of ExperiemntSetupControl.
	/// </summary>
	public partial class ExperiemntSetupControl : DockContent
	{
		RespEngine respEngine;
		public ExperiemntSetupControl()
		{
			TabText = "Experiemnt Control";
			InitializeComponent();
		}
		
		
		
		void ExperiemntSetupControlLoad(object sender, EventArgs e)
		{
			respEngine = RespEngine.Instance;
			textBox1.DataBindings.Add("Text", respEngine , "FlushPeriod", false, DataSourceUpdateMode.OnPropertyChanged);
			textBox2.DataBindings.Add("Text", respEngine , "WaitPeriod", false, DataSourceUpdateMode.OnPropertyChanged);
			textBox3.DataBindings.Add("Text", respEngine , "MeasurePeriod", false, DataSourceUpdateMode.OnPropertyChanged);

			textBoxSpeedInc.DataBindings.Add("Text", respEngine ,"SpeedIncrement", false, DataSourceUpdateMode.OnPropertyChanged);
			textBoxSpeedDec.DataBindings.Add("Text", respEngine ,"SpeedDecrement", false, DataSourceUpdateMode.OnPropertyChanged);
			textBoxSpeedIncCycleCount.DataBindings.Add("Text", respEngine ,"SpeedCyclesPerIncrement", false, DataSourceUpdateMode.OnPropertyChanged);

			textBoxSpeedMax.DataBindings.Add("Text", respEngine ,"MaxSpeed", false, DataSourceUpdateMode.OnPropertyChanged);
			textBoxSpeedMin.DataBindings.Add("Text", respEngine ,"MinSpeed", false, DataSourceUpdateMode.OnPropertyChanged);
			
			checkBoxSpeedIncEachCyc.DataBindings.Add("Checked", respEngine, "SpeedIncrementEnable", false, DataSourceUpdateMode.OnPropertyChanged);
			checkBoxSpeedDecFlush.DataBindings.Add("Checked", respEngine, "SpeedDecrementOnFlush", false, DataSourceUpdateMode.OnPropertyChanged);

		}
		
		void Button5Click(object sender, EventArgs e)
		{
			respEngine.Start();
		}
		
		
		void Button7Click(object sender, EventArgs e)
		{
			respEngine.Reset();
		}
	}
}
