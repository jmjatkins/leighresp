/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using LeighDAQclient;
using WeifenLuo.WinFormsUI.Docking;

namespace Respirometer
{
	/// <summary>
	/// Description of respPumpControl.
	/// </summary>
	public partial class RespPumpControl : DockContent, INotifyPropertyChanged
	{
		public RespPumpControl()
		{
			InitializeComponent();
			HideOnClose = true;
			InitUI();
			TabText = "Flush Pump";
			RespPumpControl.Instance = this;
		}
		
		static public RespPumpControl Instance;
		
		public void InitUI()
		{
			checkBox1.DataBindings.Add("Checked", this, "FlushPumpOn", false, DataSourceUpdateMode.OnPropertyChanged);
			//radioButton2.DataBindings.Add("Checked", this, "FlushPumpNotOn");
		}
		
		
		private bool flushPumpOn;
		public  bool FlushPumpOn {
			get { return flushPumpOn; }
			set {
				if(flushPumpOn != value) {
					flushPumpOn = value;
					if(global.leighDaq.checkConnection()) {
						global.leighDaq.SetDigOut(0, value);
					}
					OnPropertyChangedEvent("FlushPumpOn");
				}
			}
		}
		
		
		private void OnPropertyChangedEvent(string propertyName)
		{
			if(propertyChanged!=null)
				propertyChanged(this, new PropertyChangedEventArgs(propertyName) );
		}

		[NonSerialized]
		private PropertyChangedEventHandler propertyChanged;
		public event PropertyChangedEventHandler PropertyChanged {
			add {
				propertyChanged += value;
			}
			remove {
				propertyChanged -= value;
			}
		}
		

	}
}
