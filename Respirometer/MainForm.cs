/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using LeighDAQclient;
//using MicroxOxyMeter;

namespace Respirometer
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		private DeserializeDockContent m_deserializeDockContent;

		public MainForm()
		{
			InitializeComponent();
			m_deserializeDockContent = new DeserializeDockContent(GetContentFromPersistString);
			Text = "Leigh Resp Ver " + Application.ProductVersion;
			/*
			global.temperatureControl.Show(dockPanel1);
			global.respPumpControl.Show(dockPanel1);
			global.respMotorControl.Show(dockPanel1);
			global.respMainPannelControl0.Show(dockPanel1);
			global.respMainPannelControl1.Show(dockPanel1);
			//global.respMainPannelControl2.Show(dockPanel1);
			//global.respMainPannelControl3.Show(dockPanel1);
			global.experiemntSetupControl.Show(dockPanel1);
			global.statusBanner.Show(dockPanel1);
			 */
		}
		
		void AddWindow(DockContent dc)
		{
			//viewToolStripMenuItem.DropDownItems.Clear();
			ToolStripMenuItem m =new ToolStripMenuItem(dc.TabText);
			m.Tag = dc;
			m.Click += new EventHandler(MenuItemClickHandler);
			viewToolStripMenuItem.DropDownItems.Add(m);
		}
		
		
		private IDockContent GetContentFromPersistString(string persistString)
		{
			if (persistString == typeof(TemperatureControl).ToString())
				return global.temperatureControl ;
			if (persistString == typeof(StatusBanner).ToString())
				return global.statusBanner;
			else if (persistString == typeof(RespPumpControl).ToString())
				return global.respPumpControl;
			else if (persistString == "RespMainPannelControl0")
				return global.respMainPannelControl0;
			else if (persistString == "RespMainPannelControl1")
				return global.respMainPannelControl1;
			else if (persistString == "RespMainPannelControl2")
				return global.respMainPannelControl2;
			else if (persistString == "RespMainPannelControl3")
				return global.respMainPannelControl3;
			
			else if (persistString == typeof(RespMotorControl).ToString())
				return global.respMotorControl;
			else if (persistString == typeof(ExperiemntSetupControl).ToString())
				return global.experiemntSetupControl;
			else return null;
		}
		
		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			string configFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "DockPanel.config");
			dockPanel1.SaveAsXml(configFile, System.Text.Encoding.ASCII);
		}

		void MainFormLoad(object sender, EventArgs e)
		{
			string configFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "DockPanel.config");

			if(System.IO.File.Exists(configFile)){
				dockPanel1.LoadFromXml(configFile, m_deserializeDockContent );
			}
			
			AddWindow(global.temperatureControl);
			AddWindow(global.respPumpControl);
			AddWindow(global.respMotorControl);
			AddWindow(global.respMainPannelControl0);
			AddWindow(global.respMainPannelControl1);
			AddWindow(global.experiemntSetupControl);
			AddWindow(global.statusBanner);

			//dockPanel1.DockLeftPortion = 320;
			LeighOxymeter.OxyMeterService.instance.Initialise(this);
		}
		
		void LeighDaqConfigToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(global.leighDaq != null) 	global.leighDaq.CloseConnection();
			DialogForm.Show( LeighDaqInterfaceHelper.GetDeviceSelectionControl( global.leighDaq ) );
			if(LeighDaqInterfaceHelper.leighDaqInterface !=null) global.leighDaq = LeighDaqInterfaceHelper.leighDaqInterface;
		}
		
		void OxymeterConfigToolStripMenuItemClick(object sender, EventArgs e)
		{
			DialogForm.Show( RespEngine.Instance.OxymeterUI);
		}
		
		private void MenuItemClickHandler(object sender, EventArgs e)
		{
			ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;
			DockContent dc = (DockContent)clickedItem.Tag;
			dc.Show(dockPanel1);
			// Take some action based on the data in clickedItem
		}
		
		void ViewToolStripMenuItemDropDownOpening(object sender, EventArgs e)
		{
			foreach(ToolStripMenuItem mi in viewToolStripMenuItem.DropDownItems) {
				DockContent dc = (DockContent)mi.Tag;
				mi.Checked = !dc.IsHidden;
			}
		}
	}
}
