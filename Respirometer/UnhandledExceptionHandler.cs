/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace UnhandledExceptionHandler
{
	/// <summary>
	/// Handles Unhandled Exceptions On All Threads
	/// Writes To Log
	/// </summary>
	public class UnhandledExceptionHandler
	{
		static UnhandledExceptionHandler instance;
		public static void Init(string logName)
		{
			instance = new UnhandledExceptionHandler();
			instance.eventLogSourcename = logName;
		}

		UnhandledExceptionHandler()
		{
			Application.ThreadException += new ThreadExceptionEventHandler(ExceptionHandler);
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ExceptionHandler2);
		}
		
		string eventLogSourcename;
		
		private void ExceptionHandler( object sender, ThreadExceptionEventArgs e)
		{
			//EventLog.WriteEntry( eventLogSourcename, "Exception thrown : " + e.Exception.Message );
		}

		private void ExceptionHandler2( object sender, UnhandledExceptionEventArgs e )
		{
			try {
				if(e.ExceptionObject is Exception){
					Exception ex = e.ExceptionObject as Exception;
					EventLog.WriteEntry( eventLogSourcename, "Exception 2 thrown : " + ex.Message );
				}
			} catch
			{
				
			}
		}
		
	}
}
