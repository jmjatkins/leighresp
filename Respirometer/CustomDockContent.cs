/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/
 
using System;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Respirometer
{
	public class CustomDockContent : DockContent  
	{
		string persistName;
		public string PersistName {
			get { return persistName; }
		}
		
		public CustomDockContent(string name, Control control, DockState dockState, DockStyle dockStyle )
		{
			this.HideOnClose = true;
			this.Text = name;
			this.persistName = name;
			DockPadding.Top = 1;
			control.Dock = dockStyle;
			//this.DockState = dockState;
			this.Controls.Add(control);
			this.DoubleBuffered = true;
		}
		
		public CustomDockContent(string name, Control control, DockState dockState, DockAreas dockAreas, DockStyle dockStyle ) 
			: this(name, control, dockState, dockStyle )
		{
			this.DockAreas = dockAreas; 
		}
			
		protected override string GetPersistString()
		{
			return  persistName;
		}
	}
}
