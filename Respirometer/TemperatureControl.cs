/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Respirometer
{
	/// <summary>
	/// Description of TemperatureControl.
	/// </summary>
	public partial class TemperatureControl : DockContent, INotifyPropertyChanged
	{
		static public TemperatureControl Instance;

		public TemperatureControl()
		{
			InitializeComponent();
			HideOnClose = true;
			TabText = "Temperature";
			InitUI();
			TemperatureControl.Instance = this;
		}

		public void InitUI()
		{
			checkBox1.DataBindings.Add("Checked", this, "TempControlEnabled", false, DataSourceUpdateMode.OnPropertyChanged);
			checkBox3.DataBindings.Add("Checked", this, "HeatingMode", false, DataSourceUpdateMode.OnPropertyChanged);
			textBox1.DataBindings.Add("Text", this, "SetTemperature", false, DataSourceUpdateMode.OnValidation);
			checkBox2.DataBindings.Add("Checked", this, "TempControlPumpOn", false, DataSourceUpdateMode.OnPropertyChanged);
			textBox1.DataBindings.Add("Enabled", this, "TempControlEnabled", false, DataSourceUpdateMode.OnPropertyChanged);
		}
		
		bool tempControlEnabled;
		public bool TempControlEnabled {
			get { return tempControlEnabled; }
			set { 
				OnPropertyChangedEvent("TempControlEnabled");
				tempControlEnabled = value; 
			}
		}
		
		double setTemperature;
		public double SetTemperature {
			get { return setTemperature; }
			set { 
				setTemperature = value; 
				OnPropertyChangedEvent("SetTemperature");
			}
		}
		
		bool heatingMode;
		public bool HeatingMode {
			get { return heatingMode; }
			set { 
				heatingMode = value; 
				OnPropertyChangedEvent("HeatingMode");
			}
		}
		
		private bool tempControlPumpOn;
		public  bool TempControlPumpOn {
			get { return tempControlPumpOn; }
			set {
				if(tempControlPumpOn != value) {
					tempControlPumpOn = value;
					if(global.leighDaq.checkConnection()) {
						global.leighDaq.SetDigOut(1, value);
					}
					OnPropertyChangedEvent("HeaterOn");
				}
			}
		}
		
		private void OnPropertyChangedEvent(string propertyName)
		{
			if(propertyChanged!=null)
				propertyChanged(this, new PropertyChangedEventArgs(propertyName) );
		}
		
		[NonSerialized]
		private PropertyChangedEventHandler propertyChanged;
		public event PropertyChangedEventHandler PropertyChanged {
			add {
				propertyChanged += value;
			}
			remove {
				propertyChanged -= value;
			}
		}
		
		
	}
}
