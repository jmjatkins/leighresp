/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.Threading;
using System.Windows.Forms;
using LeighDAQclient;
using LeighOxymeter;

namespace Respirometer
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		/// 
		
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			global.initialise();
			
			MainForm mainForm = new MainForm();
			RespEngine.Instance.FlushPeriod  = 9;
			
			
			try {
				global.leighDaq = LeighDaqInterfaceHelper.getDevice( XmlAppSettings.Settings.GetSetting("LeighDaqLastUsedAddress", "" ), mainForm );
			}
			catch {
				
			}
			RespEngine.Instance.Initialise( );
			Application.Run(mainForm);
			RespEngine.Instance.SaveState();
			try {
				XmlAppSettings.Settings.PutSetting("LeighDaqLastUsedAddress", global.leighDaq.getAdress() );
			}
			catch(Exception) {
			}
		}
	}
}
