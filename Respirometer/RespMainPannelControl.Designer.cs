/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Respirometer
{
	partial class RespMainPannelControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RespMainPannelControl));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.tChart1 = new Steema.TeeChart.TChart();
			this.points2 = new Steema.TeeChart.Styles.Points();
			this.line2 = new Steema.TeeChart.Styles.Line();
			this.line1 = new Steema.TeeChart.Styles.Line();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 595);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(623, 10);
			this.panel1.TabIndex = 0;
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.SystemColors.Control;
			this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panel5.Location = new System.Drawing.Point(0, 0);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(623, 10);
			this.panel5.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel4);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 10);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(623, 585);
			this.panel2.TabIndex = 3;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.tChart1);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(475, 585);
			this.panel4.TabIndex = 1;
			// 
			// tChart1
			// 
			// 
			// 
			// 
			this.tChart1.Aspect.ElevationFloat = 345;
			this.tChart1.Aspect.RotationFloat = 345;
			this.tChart1.Aspect.View3D = false;
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.AutomaticMaximum = false;
			this.tChart1.Axes.Bottom.AutomaticMinimum = false;
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.Grid.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.tChart1.Axes.Bottom.Grid.Style = System.Drawing.Drawing2D.DashStyle.Dash;
			this.tChart1.Axes.Bottom.Grid.Visible = false;
			this.tChart1.Axes.Bottom.Grid.ZPosition = 0;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.Labels.Font.Shadow.Visible = false;
			this.tChart1.Axes.Bottom.Labels.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.Labels.Shadow.Visible = false;
			this.tChart1.Axes.Bottom.Maximum = 24;
			this.tChart1.Axes.Bottom.Minimum = 0;
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.MinorTicks.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.Ticks.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.TicksInner.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.tChart1.Axes.Bottom.TicksInner.Visible = false;
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.Title.Caption = "Time (s)";
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.Title.Font.Shadow.Visible = false;
			this.tChart1.Axes.Bottom.Title.Font.Unit = System.Drawing.GraphicsUnit.World;
			this.tChart1.Axes.Bottom.Title.Lines = new string[] {
						"Time (s)"};
			// 
			// 
			// 
			this.tChart1.Axes.Bottom.Title.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Axes.Depth.Automatic = true;
			// 
			// 
			// 
			this.tChart1.Axes.Depth.Grid.Style = System.Drawing.Drawing2D.DashStyle.Dash;
			this.tChart1.Axes.Depth.Grid.ZPosition = 0;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Depth.Labels.Font.Shadow.Visible = false;
			this.tChart1.Axes.Depth.Labels.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.Depth.Labels.Shadow.Visible = false;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Depth.Title.Font.Shadow.Visible = false;
			this.tChart1.Axes.Depth.Title.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.Depth.Title.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Axes.DepthTop.Automatic = true;
			// 
			// 
			// 
			this.tChart1.Axes.DepthTop.Grid.Style = System.Drawing.Drawing2D.DashStyle.Dash;
			this.tChart1.Axes.DepthTop.Grid.ZPosition = 0;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.DepthTop.Labels.Font.Shadow.Visible = false;
			this.tChart1.Axes.DepthTop.Labels.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.DepthTop.Labels.Shadow.Visible = false;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.DepthTop.Title.Font.Shadow.Visible = false;
			this.tChart1.Axes.DepthTop.Title.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.DepthTop.Title.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Axes.Left.AutomaticMaximum = false;
			this.tChart1.Axes.Left.AutomaticMinimum = false;
			// 
			// 
			// 
			this.tChart1.Axes.Left.Grid.Style = System.Drawing.Drawing2D.DashStyle.Dash;
			this.tChart1.Axes.Left.Grid.ZPosition = 0;
			this.tChart1.Axes.Left.Increment = 1;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Left.Labels.Font.Shadow.Visible = false;
			this.tChart1.Axes.Left.Labels.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.Left.Labels.Shadow.Visible = false;
			this.tChart1.Axes.Left.Maximum = 120;
			this.tChart1.Axes.Left.Minimum = 0;
			// 
			// 
			// 
			this.tChart1.Axes.Left.Title.Caption = "Oygen (% sat)";
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Left.Title.Font.Shadow.Visible = false;
			this.tChart1.Axes.Left.Title.Font.Unit = System.Drawing.GraphicsUnit.World;
			this.tChart1.Axes.Left.Title.Lines = new string[] {
						"Oygen (% sat)"};
			// 
			// 
			// 
			this.tChart1.Axes.Left.Title.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Axes.Right.Automatic = true;
			// 
			// 
			// 
			this.tChart1.Axes.Right.Grid.Style = System.Drawing.Drawing2D.DashStyle.Dash;
			this.tChart1.Axes.Right.Grid.ZPosition = 0;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Right.Labels.Font.Shadow.Visible = false;
			this.tChart1.Axes.Right.Labels.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.Right.Labels.Shadow.Visible = false;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Right.Title.Font.Shadow.Visible = false;
			this.tChart1.Axes.Right.Title.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.Right.Title.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Axes.Top.AutomaticMaximum = false;
			this.tChart1.Axes.Top.AutomaticMinimum = false;
			// 
			// 
			// 
			this.tChart1.Axes.Top.Grid.Style = System.Drawing.Drawing2D.DashStyle.Dash;
			this.tChart1.Axes.Top.Grid.ZPosition = 0;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Top.Labels.Font.Shadow.Visible = false;
			this.tChart1.Axes.Top.Labels.Font.Size = 6;
			this.tChart1.Axes.Top.Labels.Font.SizeFloat = 6F;
			this.tChart1.Axes.Top.Labels.Font.Unit = System.Drawing.GraphicsUnit.World;
			this.tChart1.Axes.Top.Labels.Separation = 1;
			// 
			// 
			// 
			this.tChart1.Axes.Top.Labels.Shadow.Visible = false;
			this.tChart1.Axes.Top.Maximum = 24;
			this.tChart1.Axes.Top.Minimum = 0;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Axes.Top.Title.Font.Shadow.Visible = false;
			this.tChart1.Axes.Top.Title.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Axes.Top.Title.Shadow.Visible = false;
			this.tChart1.Cursor = System.Windows.Forms.Cursors.Default;
			this.tChart1.Dock = System.Windows.Forms.DockStyle.Fill;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Footer.Font.Shadow.Visible = false;
			this.tChart1.Footer.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Footer.Shadow.Visible = false;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Header.Font.Shadow.Visible = false;
			this.tChart1.Header.Font.Unit = System.Drawing.GraphicsUnit.World;
			this.tChart1.Header.Lines = new string[] {
						"TeeChart"};
			// 
			// 
			// 
			this.tChart1.Header.Shadow.Visible = false;
			this.tChart1.Header.Visible = false;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Legend.Font.Shadow.Visible = false;
			this.tChart1.Legend.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Legend.Title.Font.Bold = true;
			// 
			// 
			// 
			this.tChart1.Legend.Title.Font.Shadow.Visible = false;
			this.tChart1.Legend.Title.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.Legend.Title.Pen.Visible = false;
			// 
			// 
			// 
			this.tChart1.Legend.Title.Shadow.Visible = false;
			this.tChart1.Legend.Visible = false;
			this.tChart1.Location = new System.Drawing.Point(0, 0);
			this.tChart1.Name = "tChart1";
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Panel.Bevel.Outer = Steema.TeeChart.Drawing.BevelStyles.None;
			// 
			// 
			// 
			this.tChart1.Panel.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			// 
			// 
			// 
			this.tChart1.Panel.Gradient.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.tChart1.Panel.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.tChart1.Panel.Gradient.Visible = true;
			// 
			// 
			// 
			this.tChart1.Panel.Shadow.Visible = false;
			this.tChart1.Series.Add(this.points2);
			this.tChart1.Series.Add(this.line2);
			this.tChart1.Series.Add(this.line1);
			this.tChart1.Size = new System.Drawing.Size(475, 585);
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.SubFooter.Font.Shadow.Visible = false;
			this.tChart1.SubFooter.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.SubFooter.Shadow.Visible = false;
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.SubHeader.Font.Shadow.Visible = false;
			this.tChart1.SubHeader.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			this.tChart1.SubHeader.Shadow.Visible = false;
			this.tChart1.TabIndex = 0;
			// 
			// 
			// 
			// 
			// 
			// 
			this.tChart1.Walls.Back.AutoHide = false;
			// 
			// 
			// 
			this.tChart1.Walls.Back.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// 
			// 
			this.tChart1.Walls.Back.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Walls.Bottom.AutoHide = false;
			// 
			// 
			// 
			this.tChart1.Walls.Bottom.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Walls.Left.AutoHide = false;
			// 
			// 
			// 
			this.tChart1.Walls.Left.Shadow.Visible = false;
			// 
			// 
			// 
			this.tChart1.Walls.Right.AutoHide = false;
			// 
			// 
			// 
			this.tChart1.Walls.Right.Shadow.Visible = false;
			this.tChart1.Click += new System.EventHandler(this.TChart1Click);
			// 
			// points2
			// 
			this.points2.HorizAxis = Steema.TeeChart.Styles.HorizontalAxis.Top;
			// 
			// 
			// 
			this.points2.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
			// 
			// 
			// 
			// 
			// 
			// 
			this.points2.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
			this.points2.Marks.Callout.ArrowHeadSize = 8;
			// 
			// 
			// 
			this.points2.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
			this.points2.Marks.Callout.Distance = 0;
			this.points2.Marks.Callout.Draw3D = false;
			this.points2.Marks.Callout.Length = 0;
			this.points2.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
			// 
			// 
			// 
			// 
			// 
			// 
			this.points2.Marks.Font.Shadow.Visible = false;
			this.points2.Marks.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			// 
			// 
			// 
			this.points2.Pointer.Brush.Color = System.Drawing.Color.Yellow;
			// 
			// 
			// 
			this.points2.Pointer.Pen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
			this.points2.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Nothing;
			this.points2.Pointer.Visible = true;
			this.points2.Title = "Phase Marks";
			// 
			// 
			// 
			this.points2.XValues.DataMember = "X";
			this.points2.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
			// 
			// 
			// 
			this.points2.YValues.DataMember = "Y";
			// 
			// line2
			// 
			// 
			// 
			// 
			this.line2.Brush.Color = System.Drawing.Color.Red;
			this.line2.ClickableLine = false;
			this.line2.ColorEach = true;
			this.line2.HorizAxis = Steema.TeeChart.Styles.HorizontalAxis.Both;
			// 
			// 
			// 
			this.line2.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.line2.LinePen.Width = 2;
			// 
			// 
			// 
			// 
			// 
			// 
			this.line2.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
			this.line2.Marks.Callout.ArrowHeadSize = 8;
			// 
			// 
			// 
			this.line2.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
			this.line2.Marks.Callout.Distance = 0;
			this.line2.Marks.Callout.Draw3D = false;
			this.line2.Marks.Callout.Length = 10;
			this.line2.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
			// 
			// 
			// 
			// 
			// 
			// 
			this.line2.Marks.Font.Shadow.Visible = false;
			this.line2.Marks.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			// 
			// 
			// 
			this.line2.Pointer.Brush.Color = System.Drawing.Color.Red;
			this.line2.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
			this.line2.Title = "O2";
			// 
			// 
			// 
			this.line2.XValues.DataMember = "X";
			this.line2.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
			// 
			// 
			// 
			this.line2.YValues.DataMember = "Y";
			// 
			// line1
			// 
			// 
			// 
			// 
			this.line1.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			// 
			// 
			// 
			this.line1.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(115)))));
			this.line1.LinePen.Width = 2;
			// 
			// 
			// 
			// 
			// 
			// 
			this.line1.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
			this.line1.Marks.Callout.ArrowHeadSize = 8;
			// 
			// 
			// 
			this.line1.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
			this.line1.Marks.Callout.Distance = 0;
			this.line1.Marks.Callout.Draw3D = false;
			this.line1.Marks.Callout.Length = 10;
			this.line1.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
			// 
			// 
			// 
			// 
			// 
			// 
			this.line1.Marks.Font.Shadow.Visible = false;
			this.line1.Marks.Font.Unit = System.Drawing.GraphicsUnit.World;
			// 
			// 
			// 
			// 
			// 
			// 
			this.line1.Pointer.Brush.Color = System.Drawing.Color.Green;
			this.line1.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
			this.line1.Title = "Regression";
			// 
			// 
			// 
			this.line1.XValues.DataMember = "X";
			this.line1.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
			// 
			// 
			// 
			this.line1.YValues.DataMember = "Y";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label17);
			this.panel3.Controls.Add(this.label16);
			this.panel3.Controls.Add(this.label15);
			this.panel3.Controls.Add(this.label12);
			this.panel3.Controls.Add(this.label14);
			this.panel3.Controls.Add(this.label13);
			this.panel3.Controls.Add(this.label11);
			this.panel3.Controls.Add(this.label10);
			this.panel3.Controls.Add(this.label9);
			this.panel3.Controls.Add(this.label8);
			this.panel3.Controls.Add(this.label7);
			this.panel3.Controls.Add(this.label6);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel3.Location = new System.Drawing.Point(475, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(148, 585);
			this.panel3.TabIndex = 0;
			this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel3Paint);
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(39, 295);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(100, 23);
			this.label17.TabIndex = 11;
			this.label17.Text = "0.00";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(22, 280);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(100, 15);
			this.label16.TabIndex = 10;
			this.label16.Text = "R²";
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(39, 242);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(100, 23);
			this.label15.TabIndex = 9;
			this.label15.Text = "0.00";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(22, 227);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(100, 15);
			this.label12.TabIndex = 8;
			this.label12.Text = "Last dPO2/dt";
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(39, 194);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(100, 23);
			this.label14.TabIndex = 7;
			this.label14.Text = "99";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(18, 179);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(100, 15);
			this.label13.TabIndex = 6;
			this.label13.Text = "Time Until Next (s)";
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(39, 147);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(100, 23);
			this.label11.TabIndex = 5;
			this.label11.Text = "0";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(18, 132);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(100, 15);
			this.label10.TabIndex = 4;
			this.label10.Text = "Cycle Count";
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(39, 100);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(100, 23);
			this.label9.TabIndex = 3;
			this.label9.Text = "0.00";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(18, 85);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(121, 15);
			this.label8.TabIndex = 2;
			this.label8.Text = "Chamber Temp (deg C)";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(39, 49);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 23);
			this.label7.TabIndex = 1;
			this.label7.Text = "0.00";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(22, 34);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(70, 15);
			this.label6.TabIndex = 0;
			this.label6.Text = "Chamber O2";
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// RespMainPannelControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(623, 605);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel5);
			this.Controls.Add(this.panel1);
			this.DoubleBuffered = true;
			this.Name = "RespMainPannelControl";
			this.Load += new System.EventHandler(this.RespMainPannelControlLoad);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RespMainPannelControlFormClosing);
			this.panel2.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label15;
		private Steema.TeeChart.Styles.Line line2;
		private Steema.TeeChart.Styles.Points points2;
		private Steema.TeeChart.Styles.Line line1;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Panel panel5;
		private Steema.TeeChart.TChart tChart1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
	}
}
