/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of TcpCommsObject.
	/// </summary>
	public class TcpCommsObject : CommsObject
	{
		TcpClient tcpClient;
		const int XPORT_PORT = 30718;
		Thread rxThread;
		
		public TcpCommsObject()
		{
			tcpClient = new TcpClient();
			tcpClient.ReceiveTimeout = 10000;
			rxThread = new Thread(new ThreadStart(this.RecieveThreadLoop));
		}
		
		public static string[] GetAvailableAddresses()
		{
			List<string> addresses = new List<string>();

			UdpClient udpClient = new UdpClient();
			udpClient.EnableBroadcast = true;
			udpClient.Client.ReceiveTimeout = 200;
			
			byte[] data  = new byte[]{0,0,0,0xF6};
			udpClient.Send(data, data.Length, "130.216.86.255" ,XPORT_PORT);

			System.Net.IPEndPoint ipe = new System.Net.IPEndPoint(new System.Net.IPAddress(new byte[]{130,216,86,255} ),XPORT_PORT);
			while(true)
			{
				try{
					udpClient.Receive( ref ipe);
					addresses.Add(ipe.Address.ToString());
				}
				catch(SocketException ex)
				{
					if(ex.SocketErrorCode == SocketError.TimedOut)
						break;
					else throw ex;
				}
			}
			return addresses.ToArray();
		}

		private string portName;
		public override string PortName {
			get { return portName; }
			set { portName = value; }
		}
		
		public override void Open()
		{
			tcpClient = new TcpClient();
			tcpClient.ReceiveTimeout = 10000;
			tcpClient.Connect(portName, 10001);
			rxThread.Start();
		}
		
		public override void Close()
		{
			tcpClient.Close();
			tcpClient = null;
		}
		
		public override int BytesToRead {
			get {
				return tcpClient.ReceiveBufferSize;
			}
		}
		
		public override void DiscardInBuffer()
		{
			try {
				tcpClient.GetStream().Flush();
			}
			catch
			{
				
			}
		}
		
		public override void Write(byte[] data, int offset, int length)
		{
			try {
				tcpClient.GetStream().Write(data, offset, length);
			}
			catch
			{
				
			}
		}
		
		public override int Read(byte[] data, int offset, int length)
		{
			return tcpClient.GetStream().Read(data, offset, length);
		}
		
		public override bool IsOpen {
			get { return tcpClient.Connected; }
		}
		
		private void RecieveThreadLoop()
		{
			byte[] buf = new byte[]{};
			while ((tcpClient!=null) && (tcpClient.Connected)) {
				if(tcpClient.Available >0)
					OnDataReceived(this, EventArgs.Empty);
				else Thread.Sleep(100);
			}
		}
		
		public override event EventHandler DataReceived;
		private void OnDataReceived(object o, EventArgs e)
		{
			if(DataReceived != null)
			{
				DataReceived(o, e);
			}
		}
		
	}
}
