/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of crc.
	/// </summary>
	public static class crc
	{
		static ushort CRCByte(byte Data, uint crc)
		{
			unchecked{
			byte Buffer,Buffer2,Buffer3;

			Buffer = (byte)(Data ^ (byte)(crc % 256));

			Buffer = (byte)(Buffer ^ (Buffer * 16));

			Buffer2 = (byte)((Buffer / 32) ^ Buffer);

			Buffer3 = (byte)(crc / 256);

			crc = (uint)((crc & 0x00FF) | ((uint)(Buffer2) * 256));
			Buffer3 = (byte) ((Buffer * 8) ^ Buffer3);

			Buffer3 = (byte)( Buffer3 ^ (byte)(Buffer / 16));
			return (ushort)((Buffer3) | (crc & 0xFF00));
			}
		}

		public static ushort calc(byte[] data, int count)
		{
			ushort crc = 0xFFFF;
			for(int i=0; i<count; i++)
			{
				crc = CRCByte( data[i], crc );
			}
			return crc;
		}
		
		
		
		
		
	}
}
