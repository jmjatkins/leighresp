﻿/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of LeighDaqInterfaceControl.
	/// </summary>
	public partial class LeighDaqInterfaceControl : UserControl
	{
		public LeighDaqInterfaceControl()
		{
			InitializeComponent();
			comboBox1.Items.Clear();
			try {
				string[] ports = LeighDaqInterfaceHelper.FindAllAttchedDevices();
				comboBox1.Items.AddRange(ports);
			}
			catch(Exception ex)
			{
				MessageBox.Show("Leigh Daq Scan Failed With: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		
		void Button2Click(object sender, EventArgs e)
		{
			try {
				LeighDaqInterfaceHelper.getDevice(comboBox1.Text, this.Parent );
				label2.Text = "Status: Connected";
			}
			catch(Exception ex) {
				MessageBox.Show("Error Opening Connection\n" + ex.Message, "Error Opening Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
				label2.Text = "Status: Disconnected";
				return;
			}
			this.ParentForm.Close();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			if(LeighDaqInterfaceHelper.leighDaqInterface != null)
			{
				Form f = new Form();
				f.Controls.Add(LeighDaqInterfaceHelper.leighDaqInterface.GetConfigControl());
				f.ShowDialog();
			}
		}
		
		void LeighDaqInterfaceControlLoad(object sender, EventArgs e)
		{
			try {
				comboBox1.Text = LeighDaqInterfaceHelper.leighDaqInterface.getAdress();
			}
			catch {};
		}
	}
}
