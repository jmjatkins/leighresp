﻿/* 
 * 	LeighResp Software
 *
 *	Copyright (C) 2011-2016, John Atkins
 *
 *
 *	This file is part of the LeighResp software. LeighResp is an respirometry system
 *	intended to help with the automation of respiration measurements. This component of the
 *	LeighResp project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The LeighResp software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO.Ports;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of LeighDaqUsbNetpro.
	/// </summary>
	public class LeighDaqUsbNetPro : LeighDaqInterface
	{
		SerialPort sPort = new SerialPort();
		
		string port;
		public LeighDaqUsbNetPro(string port)
		{
			this.port = port;
			string p = port.Split(' ')[0];
			sPort.PortName = p;
			sPort.WriteTimeout = 1000;
			sPort.Open();
		}
		
		
		static public string[] GetPortNames()
		{
			List<string> portNames = new List<string>();
			portNames.AddRange ( SerialCommsObject.GetAvailableAddresses());
			List<string> USBNetNames = new List<string>();
			foreach(string s in portNames) {
				USBNetNames.Add(s + " USBNetPro");
			}
			return USBNetNames.ToArray();
		}
		
		
		override public void SetDigOut(byte channel, bool value) {
			try{
				sPort.WriteLine(String.Format("p{0}={1}", channel+1, value ? "1" : "0"));
			}
			catch {
				sPort.Close();
			}
		}

		override public void CloseConnection()
		{
			sPort.Close();
		}

		override public bool checkConnection() {
			
			try{
				if(!sPort.IsOpen) sPort.Open();
			}
			catch {
				
			}
			return sPort.IsOpen;
		}

		override public Control GetConfigControl() {
			Label l = new Label();
			l.Text = "USBNetPro : "+ sPort.PortName;
			l.Dock = DockStyle.Fill;
			return l;
		}

		override public string getAdress() {
			return port;
		}
		
	}
}
