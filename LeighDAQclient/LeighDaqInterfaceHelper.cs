﻿/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of LeighDaqInterfaceHelper.
	/// </summary>
	public static class LeighDaqInterfaceHelper
	{
		public static string[] FindAllAttchedDevices()
		{
			List<string> devices = new List<string>();
			devices.AddRange( LeighDAQ.GetPortNames() );
			devices.AddRange( LeighDaqUsbNet.GetPortNames() );
			devices.AddRange( LeighDaqUsbNetPro.GetPortNames() );
			return devices.ToArray();
		}

		static LeighDaqInterfaceControl ldic = new LeighDaqInterfaceControl();
		static public Control GetDeviceSelectionControl( LeighDaqInterface ldi )
		{
			//if(ldi == null)
			return ldic;
			//else  return ldi.GetConfigControl();
		}
		
		
		static public LeighDaqInterface leighDaqInterface;
		static public LeighDaqInterface getDevice(string address, Control sycnControl)
		{
			
			foreach(string s in LeighDaqUsbNet.GetPortNames())
			{
				if(s==address){
					leighDaqInterface = new LeighDaqUsbNet(address);
					return leighDaqInterface;
				}
			}
			
			foreach(string s in LeighDaqUsbNetPro.GetPortNames())
			{
				if(s==address){
					leighDaqInterface = new LeighDaqUsbNetPro(address);
					return leighDaqInterface;
				}
			}


			foreach(string s in LeighDAQ.GetPortNames())
			{
				if(s==address){
					LeighDAQ i = new LeighDAQ();
					i.SetSycnControl(sycnControl);
					i.OpenConnection(address);
					leighDaqInterface = i;
					return i;
				}
			}
			
			throw new Exception("Device " + address + " not found");
		}


	}
}
