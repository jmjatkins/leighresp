﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 4/06/2014
 * Time: 11:31 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;


namespace LeighDAQclient
{
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class LeighDaqPwrUsb :LeighDaqInterface
	{
		
		[DllImport("PwrUSBDll.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int InitPowerUSB(out int model, StringBuilder firmwareVersion);

		[DllImport("PwrUSBDll.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ClosePowerUSB();

		[DllImport("PwrUSBDll.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SetCurrentPowerUSB(int count);

		[DllImport("PwrUSBDll.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int CheckStatusPowerUSB();

		[DllImport("PwrUSBDll.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SetPortPowerUSB(int port1_power, int port2_power, int port3_power);

		
		int address;
		
		static public string[] GetPortNames()
		{

			int noOfDevices;
			int model;
			StringBuilder firmwareVersion = new StringBuilder();
			noOfDevices = InitPowerUSB(out model, firmwareVersion);
			List<string> deviceNames = new List<string>();
			for(int i=0;i<noOfDevices;i++) {
				deviceNames.Add( String.Format( "PwrUsb{0}", i) );
			}
			return deviceNames.ToArray();
		}

		public LeighDaqPwrUsb(string address)
		{
			address = address.Remove(0,6);
			this.address = Convert.ToInt16( address );
			SetCurrentPowerUSB(this.address);
		}

		
		int[] portState = new int[3];
		
		override public void SetDigOut(byte channel, bool value){
			portState[2 - channel] = value ? 1 : 0;
			SetPortPowerUSB(portState[0], portState[1], portState[2]);
		}

		override public bool checkConnection() {
			return CheckStatusPowerUSB() > this.address;
		}

		override public Control GetConfigControl() {
			return new Control();
		}

		override public string getAdress()
		{
			return String.Format( "PwrUsb{0}", this.address);
		}

	}
}