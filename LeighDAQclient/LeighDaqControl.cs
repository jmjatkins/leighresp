/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of LeighDaqControl.
	/// </summary>
	public partial class LeighDaqControl : UserControl
	{
		LeighDAQ leighDAQ;
		public LeighDAQ LeighDAQ
		{
			set {
				leighDAQ = value;
			}
		}
		
		public LeighDaqControl()
		{
			InitializeComponent();
			initUI();
		}

		private void initUI()
		{
			comboBox1.Items.Clear();
			try {
				string[] ports = LeighDAQ.GetPortNames();
				comboBox1.Items.AddRange(ports);
			}
			catch
			{
				
			}
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			button2.Enabled = false;
			try {
				if(leighDAQ.IsConnected) leighDAQ.CloseConnection();
			}
			catch {
			}
			try {
				leighDAQ.OpenConnection( comboBox1.Text );
				label2.Text = "Status: Connected";
			}
			catch(Exception ex) {
				MessageBox.Show("Error Opening Connection\n" + ex.Message, "Error Opening Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
				label2.Text = "Status: Disconnected";
			}
			button2.Enabled = true;
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			if(leighDAQ!=null && leighDAQ.IsConnected) {
				byte[] data = {0,0x00,0x08};
				leighDAQ.SendCommand(LeighDAQ.DaqCommand.STATUS_READ,data);
				listBox1.Items.Clear();
				listBox1.Items.Add( String.Format("Client Comms Errors= {0}", leighDAQ.ClientCommsErrors ));
				listBox1.Items.Add( String.Format("Comms Errors = {0}", leighDAQ.Status.commsErrorLog));
				listBox1.Items.Add( String.Format("Device Type= {0}", leighDAQ.Status.deviceType));
				listBox1.Items.Add( String.Format("Reset Run Time= {0}", leighDAQ.Status.resetRunTime));
				listBox1.Items.Add( String.Format("debug= {0}", leighDAQ.Status.debug));
			}
		}
	}
}
