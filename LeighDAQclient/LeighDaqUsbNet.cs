﻿/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of LeighDaqUsbNet.
	/// </summary>
	public class LeighDaqUsbNet : LeighDaqInterface
	{
		
		public const uint FILE_ATTRIBUTE_NORMAL = 0x00000080;
		public const uint FILE_FLAG_OVERLAPPED = 0x40000000;
		public const uint GENERIC_READ = 0x80000000;
		public const uint GENERIC_WRITE = 0x40000000;
		public const uint OPEN_EXISTING = 3;
		public const Int32 INVALID_HANDLE_VALUE = -1;
		public const uint FILE_DEVICE_UNKNOWN = 0x00000022;
		public const uint IOCTL_UNKNOWN_BASE = FILE_DEVICE_UNKNOWN;
		public const uint METHOD_BUFFERED = 0;
		public const uint FILE_READ_ACCESS = 0x0001;
		public const uint FILE_WRITE_ACCESS = 0x0002;
		public const uint FILE_ANY_ACCESS = 0x0000;
		
		[DllImport("kernel32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		public static extern Microsoft.Win32.SafeHandles.SafeFileHandle CreateFile(
			string lpFileName,
			uint dwDesiredAccess,
			uint dwShareMode,
			IntPtr SecurityAttributes,
			uint dwCreationDisposition,
			uint dwFlagsAndAttributes,
			IntPtr hTemplateFile
		);
		
		[DllImport("Kernel32.dll", SetLastError = true)]
		public static extern bool DeviceIoControl(
			SafeFileHandle hDevice,
			uint IoControlCode,
			byte[] InBuffer,
			int nInBufferSize,
			byte[] OutBuffer,
			int nOutBufferSize,
			out int pBytesReturned,
			IntPtr Overlapped
		);
		
		protected static uint CTL_CODE(uint DeviceType, uint Function, uint Method, uint Access)
		{
			return ((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method);
		}
		
		Microsoft.Win32.SafeHandles.SafeFileHandle handle;
		const UInt32 USB2SER_IOCTL_INDEX = 0x0800;
		string port;
		
		public LeighDaqUsbNet(string port)
		{
			this.port = port;
			
			uint GP1_OUTPUT_ENABLE = CTL_CODE(FILE_DEVICE_UNKNOWN,
			                                  USB2SER_IOCTL_INDEX+21,
			                                  METHOD_BUFFERED,
			                                  FILE_ANY_ACCESS);
			byte[] enable = {1};
			int bytesReturned;
			
			string p = "\\\\.\\" + port.Split(' ')[0];
			
			handle = CreateFile( p, GENERIC_READ | GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, IntPtr.Zero);
			if(!handle.IsInvalid) {
				DeviceIoControl(handle, GP1_OUTPUT_ENABLE, enable, 1, null, 0, out bytesReturned, System.IntPtr.Zero);
			}
		}
		
		static public string[] GetPortNames()
		{
			List<string> portNames = new List<string>();
			portNames.AddRange ( SerialCommsObject.GetAvailableAddresses());
			List<string> USBNetNames = new List<string>();
			foreach(string s in portNames) {
				USBNetNames.Add(s + " USBNet");
			}
			return USBNetNames.ToArray();
		}
		
		override public void CloseConnection()
		{
			handle.Close();
			handle.SetHandleAsInvalid();
		}

		override public void SetDigOut(byte channel, bool value) {
			//TODO - handle other channels
			if(channel == 0) {
				int bytesReturned;
				uint GP1_SET_VALUE = CTL_CODE(FILE_DEVICE_UNKNOWN,
				                              USB2SER_IOCTL_INDEX+23,
				                              METHOD_BUFFERED,
				                              FILE_ANY_ACCESS);

				byte[] enable = { value ? (byte)1 : (byte)0 };
				DeviceIoControl(handle, GP1_SET_VALUE, enable, 1, null, 0, out bytesReturned, System.IntPtr.Zero);
			}
		}

		override public bool checkConnection() {
			return !handle.IsInvalid;
		}

		override public Control GetConfigControl() {
			Label l = new Label();
			l.Text = "USBNet : "+ port;
			l.Dock = DockStyle.Fill;
			return l;
		}

		override public string getAdress() {
			return port;
		}
		
	}
}
