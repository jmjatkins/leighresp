/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Runtime.Serialization;
using System.IO;
using System.Collections.Generic;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of binarySerializer.
	/// </summary>
	static public class Serializer
	{
		public class SerializerArraySizeAttribute: Attribute
		{
			public SerializerArraySizeAttribute(int arraySize)
			{
				this.arraySize = arraySize;
			}
			private int arraySize;
			public int ArraySize
			{
				get {
					return arraySize;
				}
			}
		}
		
		static public byte[] serializeParams(params object[] paramlist)
		{
			MemoryStream buf = new MemoryStream();
			foreach(object o in paramlist)
			{
				serialize(o, buf);
			}
			return buf.ToArray();
		}
		
		static private void serialize( object val, MemoryStream buf )
		{
			Type T = val.GetType();
			if(T == typeof(byte))
			{
				buf.WriteByte((byte)val);
			}
			else if(T == typeof(bool))
			{
				buf.WriteByte( Convert.ToByte( (bool)val));
			}
			else if(T == typeof(ushort))
			{
				byte[] byteBuf = BitConverter.GetBytes((ushort)val);
				buf.Write(byteBuf,0,byteBuf.Length);
			}
			else if(T == typeof(short))
			{
				byte[] byteBuf = BitConverter.GetBytes((short)val);
				buf.Write(byteBuf,0,byteBuf.Length);
			}
			else if(T == typeof(int))
			{
				byte[] byteBuf = BitConverter.GetBytes((int)val);
				buf.Write(byteBuf,0,byteBuf.Length);
			}
		}
		
		static private object deserialize(Type T, MemoryStream buf )
		{
			object o = Activator.CreateInstance(T);
			if(T == typeof(byte))
			{
				o = (byte)buf.ReadByte();
			}
			else if(T == typeof(short))
			{
				o = BitConverter.ToInt16(new byte[] {(byte)buf.ReadByte(),(byte)buf.ReadByte()}, 0);
			}
			else if(T == typeof(ushort))
			{
				o = (ushort)(buf.ReadByte() + (buf.ReadByte() <<8));
			}
			else if(T == typeof(int))
			{
				o = (int )BitConverter.ToInt32(new byte[] {(byte)buf.ReadByte(),(byte)buf.ReadByte(), (byte)buf.ReadByte(), (byte)buf.ReadByte()}, 0);
			}
			else if(T == typeof(uint))
			{
				o = (uint)BitConverter.ToInt32(new byte[] {(byte)buf.ReadByte(),(byte)buf.ReadByte(), (byte)buf.ReadByte(), (byte)buf.ReadByte()}, 0);
			}
			else if(T == typeof(ushort[]))
			{
				o = (ushort)(buf.ReadByte() + (buf.ReadByte() <<8));
			}
			else throw new Exception("Invalid Type For Deserialization: " + T.Name);
			return o;
		}
		
		static public byte[] Serialise(object o)
		{
			Type theObjectType = o.GetType();
			MemoryStream buf = new MemoryStream();
			FieldInfo[] feilds = theObjectType.GetFields();
			foreach(FieldInfo fi in feilds)
			{
				if(!fi.IsNotSerialized)
				{
					serialize(fi.GetValue(o), buf);
				}
			}
			return buf.ToArray();;
		}
		
		static int deserialisationErrors = 0;
		
		static public object DeSerialise( Type T, byte[] data)
		{
			object o = Activator.CreateInstance(T);
			MemoryStream buf = new MemoryStream(data);
			FieldInfo[] feilds = o.GetType().GetFields();
			foreach(FieldInfo fi in feilds)
			{
				if(!fi.IsNotSerialized)
				{
					try {
						fi.SetValue(o, deserialize(fi.FieldType, buf));
					}
					catch
					{
						deserialisationErrors++;
					};
				}
			}
			return o;
		}
		
	}
}
