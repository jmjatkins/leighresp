/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.IO.Ports;

namespace LeighDAQclient
{
	/// <summary>
	/// Serial Communications Object
	/// </summary>
	public class SerialCommsObject : CommsObject
	{
		System.IO.Ports.SerialPort sPort;
		
		public SerialCommsObject()
		{
			sPort= new SerialPort();
			sPort.BaudRate = 38400;
			sPort.ReadTimeout = 1000;
			sPort.WriteTimeout = 1000;
			sPort.DataReceived += OnDataReceived;
		}
		
		public static string[] GetAvailableAddresses()
		{
			return System.IO.Ports.SerialPort.GetPortNames();
		}

		public override void Open()
		{
			sPort.Open();
		}
		
		public override void Close()
		{
			sPort.Close();
		}
		
		public override int BytesToRead {
			get { return sPort.BytesToRead; }
		}
		
		public override void DiscardInBuffer()
		{
			sPort.DiscardInBuffer();
		}
		
		public override bool IsOpen {
			get { return sPort.IsOpen; }
		}
		
		public override string PortName
		{
			set {
				sPort.PortName = value;
			}
			get {
				return sPort.PortName;
			}
		}

		public override int Read(byte[] data, int offset, int length)
		{
			return sPort.Read(data, offset, length);
		}
		
		public override void Write(byte[] data, int offset, int length)
		{
			sPort.Write(data, offset, length);
		}

		public override event EventHandler DataReceived;
		private void OnDataReceived(object o, SerialDataReceivedEventArgs e)
		{
			if(DataReceived != null)
			{
				DataReceived(o, e);
			}
		}
	}
}
