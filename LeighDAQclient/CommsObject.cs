/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;

namespace LeighDAQclient
{
	/// <summary>
	/// Description of CommsObject.
	/// </summary>
	public class CommsObject
	{
/*
		public virtual string[] GetAvailableAddresses()
		{
			return new string[]{};
		}
*/
		public CommsObject()
		{
		}
		
		public virtual void ShowConfigDiaglog()
		{
			
		}

		public virtual string PortName
		{
			set {
				
			}
			get {
				return "";
			}
		}
		
		public virtual void Open()
		{
		}
		
		public virtual void Close()
		{
			
		}
		
		public virtual bool IsOpen
		{
			get {
				return false;
			}
		}
	
		public virtual void Write(byte[] data, int offset, int length)
		{
			
		}

		public virtual int Read(byte[] data, int offset, int length)
		{
			return 0;
		}
		
		public virtual int BytesToRead {
			get {
				return 0;
			}
		}

		public virtual void DiscardInBuffer()
		{
			
		}
		
		public virtual event EventHandler DataReceived;
		
	}
}
