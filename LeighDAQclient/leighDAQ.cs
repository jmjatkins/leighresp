/* 
* 	LeighResp Software 
*
*	Copyright (C) 2011-2016, John Atkins 
*
*
*	This file is part of the LeighResp software. LeighResp is an respirometry system
*	intended to help with the automation of respiration measurements. This component of the
*	LeighResp project is free software: you can redistribute it and/or modify it
*	under the terms of the GNU General Public License as published by the Free Software
*	Foundation, either version 3 of the License, or any later version.
*
*	The LeighResp software is distributed in the hope that it will be useful, but
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License along with this
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Collections.Generic;


using System.Configuration.Assemblies;
using System.Runtime.Remoting;
using System.IO;
using System.Text;
using System.ComponentModel;
using System.Resources;
using System.Runtime;
using System.Security;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;
using XmlAppSettings;


namespace LeighDAQclient
{
	/// <summary>
	/// Description of leighDAQ.
	/// </summary>
	
	public class LeighDAQ : LeighDaqInterface
	{
		const int TX_TIMEOUT_CONST = 500;
		const int TX_TIMEOUT_MULT = 1;
		
		CommsObject commsPort;
		public int ClientCommsErrors = 0;
		
		public enum DaqCommand : ushort
		{
			STATUS_READ =4,
			STATUS_RESULT =14,
			ADC_READ = 5,
			ADC_RESULT = 15,
			DAC_WRITE = 6,
			TEMP_CONTROL_READ = 1001,
			TEMO_CONTROL_READ_RESULT = 1101,
			TEMP_CONTROL_SET_TEMP = 1002,
			TEMP_CONTROL_WRITE_PID_PARAMS = 1003,
			TEMP_CONTROL_WRITE_CAL_OFFSET = 1004,
			DIG_OUTPUT_WRITE = 7,
			REBOOT = 8,
		}
		
		public class StatusPacket {
			public byte deviceType;      /*******************************************************/
			public short temperature;
			public ushort i2cErrorLog;
			public ushort commsErrorLog;    /* These feilds are shared */
			public byte setupSaved;
			public uint resetRunTime;
			public uint totalRunTime;
			public byte nvmInitGood;
			public ushort swVer;
			public int debug;
			public byte unused1;
			public byte unused2;       /*******************************************************/
		};
		
		class DaqCommandHeader
		{
			public ushort        sop;
			public ushort        id;
			public ushort        command;
			public ushort        dataLength;
			public ushort        dataCRC;
			public ushort        headerCRC;
			
			public DaqCommandHeader( )
			{
				sop = 0x1234;
			}
			
			public DaqCommandHeader( UInt16 id, DaqCommand command, UInt16 dataLength, UInt16 dataCRC )
			{
				sop = 0x1234;
				this.id = id;
				this.command = (ushort)command;
				this.dataLength = dataLength;
				this.dataCRC = dataCRC;
				this.headerCRC = 0;
			}
			
			static public int size {
				get { return 12; }
			}
			
			public bool CheckCRC()
			{
				byte[] binData = Serializer.Serialise(this);
				return  ( crc.calc( binData, binData.Length) == 0);
			}
			
			public byte[] CRCandSerialize()
			{
				byte[] binData = Serializer.Serialise(this);
				headerCRC = crc.calc( binData, binData.Length - 2);
				return Serializer.Serialise(this);
			}
		}
		/*
		private static LeighDAQ instance;
		public static LeighDAQ Instance {
			get {
				if(instance == null)
				{
					instance = new LeighDAQ();
					Thread workerThread = new Thread(  LeighDAQ.Instance.OpenConnectionQuietly  );
					workerThread.Start();
				}
				return instance;
				
			}
			set { instance = value; }
		}
		
		 */
		
		
		public LeighDAQ( )
		{
			commsPort= new CommsObject();
			rxDataDelegate = new MethodInvoker(RxData2);
		}

		
		private string address;
		public string Address {
			get { return address; }
		}
		
		override public string getAdress()
		{
			return address;
		}
		
		public void OpenConnection(string address)
		{
			CloseConnection();
			this.address = address;
			if(address.Contains("COM"))
			{
				commsPort= new  SerialCommsObject();
			}
			else commsPort = new TcpCommsObject();
			commsPort.DataReceived += RxData;
			commsPort.PortName = address;
			commsPort.Open( );
		}
		
		override public void CloseConnection()
		{
			if(commsPort.IsOpen){
				commsPort.DataReceived -= RxData;
				commsPort.Close();
			}
		}
		
		static public string[] GetPortNames()
		{
			List<string> portNames = new List<string>();
			portNames.AddRange ( SerialCommsObject.GetAvailableAddresses());
			try{
				portNames.AddRange( TcpCommsObject.GetAvailableAddresses());
			}
			catch
			{
				
			}
			
			string leighDaqLastUsedAddress = XmlAppSettings.Settings.GetSetting("LeighDaqLastUsedAddress", "" );
			if(portNames.Contains( leighDaqLastUsedAddress ))
			{
				portNames.Remove(leighDaqLastUsedAddress);
				portNames.Insert(0, leighDaqLastUsedAddress);
			}
			
			return portNames.ToArray();
		}
		
		Control sycnControl;
		public void SetSycnControl(Control sycnControl)
		{
			this.sycnControl = sycnControl;
		}
		
		MethodInvoker rxDataDelegate;
		
		void RxData(object sender, EventArgs e)
		{
			if(sycnControl !=null) {
				lock(this)
				{
					sycnControl.Invoke(rxDataDelegate);
				}
			}
		}
		
		void RxData2()
		{
			RxCommand();
		}
		
		public void TxCommand(DaqCommand command, byte[] data)
		{
			ushort dataCRC = crc.calc(data, data.Length);
			DaqCommandHeader h = new DaqCommandHeader(0,command, (ushort)data.Length, dataCRC);
			byte[] txDataHeader = h.CRCandSerialize();
			
			byte[] txData = new byte[ txDataHeader.Length + data.Length];
			txDataHeader.CopyTo(txData, 0);
			data.CopyTo(txData, txDataHeader.Length);
			commsPort.Write(txData, 0, txData.Length);
		}
		
		private int ReadPort(byte[] buffer, int offset, int length)
		{
			try {
				int timeout = TX_TIMEOUT_CONST + (length *TX_TIMEOUT_MULT);
				int startTime = System.Environment.TickCount;
				do{
					if(commsPort.BytesToRead >=length){
						return commsPort.Read(buffer, offset, length);
					} else Thread.Sleep(10);
				}while (System.Environment.TickCount - startTime < timeout);
				return 0;
			}
			catch (Exception) {
				return 0;
			}
		}
		
		public bool RxCommand()
		{
			byte[] rxData = new byte[DaqCommandHeader.size];
			int byteRead = ReadPort(rxData, 0, DaqCommandHeader.size);
			if(byteRead == DaqCommandHeader.size)
			{
				DaqCommandHeader h = (DaqCommandHeader)Serializer.DeSerialise(typeof(DaqCommandHeader), rxData);
				if(h.CheckCRC())
				{
					rxData = new byte[h.dataLength];
					if( ReadPort(rxData, 0, (int)h.dataLength ) == (int)h.dataLength )
					{
						if(crc.calc(rxData, rxData.Length) == h.dataCRC ) {
							switch((DaqCommand)h.command)
							{
									case DaqCommand.STATUS_RESULT:  ProcessStatusPacket(rxData); break;
									case DaqCommand.ADC_RESULT:  ProcessAdcPacket(rxData); break;
									case DaqCommand.TEMO_CONTROL_READ_RESULT:  ProcessTempControlPacket(rxData); break;

							}
							return true;
						} else ClientCommsErrors++;
					} else ClientCommsErrors++;
				} else ClientCommsErrors++;
			} else ClientCommsErrors++;
			return false;
		}

		public StatusPacket Status = new StatusPacket();
		
		private void ProcessStatusPacket(byte[] packet)
		{
			Status = (StatusPacket)Serializer.DeSerialise(typeof(StatusPacket) ,packet);
		}
		
		public class AdcPacket {
			public ushort channel;
			public ushort reading;
		};

		private void ProcessAdcPacket(byte[] packet)
		{
			AdcPacket adcPacket = (AdcPacket)Serializer.DeSerialise(typeof(AdcPacket),packet);
		}
		
		public event EventHandler GotTemperatureData;
		public TtempControlParams tempControlParams;
		private void ProcessTempControlPacket(byte[] packet)
		{
			tempControlParams = (TtempControlParams)Serializer.DeSerialise(typeof(TtempControlParams), packet);
			if(tempControlParams.currentTemperature0 == 0.00)
			{
				Thread.Sleep(1);
			}
			
			if(GotTemperatureData != null)
			{
				GotTemperatureData(this, EventArgs.Empty);
			}
		}
		
		public class TtempControlParams
		{
			public double currentTemperature(int channel)
			{
				switch(channel)
				{
						case 0: return (double)currentTemperature0/100;
						case 1: return (double)currentTemperature1/100;
						case 2: return (double)currentTemperature2/100;
						case 3: return (double)currentTemperature3/100;
						case 4: return (double)currentTemperature4/100;
						
						default: return 0;
				}
			}
			public double setTemperature(int channel)
			{
				switch(channel)
				{
						case 0: return (double)setTemperature0/100;
						case 1: return (double)setTemperature1/100;
						case 2: return (double)setTemperature2/100;
						case 3: return (double)setTemperature3/100;
						case 4: return (double)setTemperature4/100;
						
						default: return 0;
				}
			}

			public double calOffset(int channel)
			{
				switch(channel)
				{
						case 0: return (double)calOffset0/100;
						case 1: return (double)calOffset1/100;
						case 2: return (double)calOffset2/100;
						case 3: return (double)calOffset3/100;
						case 4: return (double)calOffset4/100;
						
						default: return 0;
				}
			}

			public int duty(int channel)
			{
				switch(channel)
				{
						case 0: return (int)duty0;
						case 1: return (int)duty1;
						case 2: return (int)duty2;
						case 3: return (int)duty3;
						case 4: return (int)duty4;
						
						default: return 0;
				}
			}
			public bool HeaterOn(int channel)
			{
				return ((heaterState & (0x01 << channel)) > 0);
			}
			
			public byte numberOfHeaters;

			public short setTemperature0;
			public short setTemperature1;
			public short setTemperature2;
			public short setTemperature3;
			public short setTemperature4;
			
			public short calOffset0;
			public short calOffset1;
			public short calOffset2;
			public short calOffset3;
			public short calOffset4;
			
			public short calScaler0;
			public short calScaler1;
			public short calScaler2;
			public short calScaler3;
			public short calScaler4;

			public short PGain;
			public short IGain;
			public short DGain;
			
			public short currentTemperature0;
			public short currentTemperature1;
			public short currentTemperature2;
			public short currentTemperature3;
			public short currentTemperature4;
			
			public ushort duty0;
			public ushort duty1;
			public ushort duty2;
			public ushort duty3;
			public ushort duty4;
			
			public byte heaterState;
		};
		
		public bool GetTemControlData()
		{
			byte[] data = {0};
			commsPort.DiscardInBuffer();
			TxCommand(LeighDAQ.DaqCommand.TEMP_CONTROL_READ, data );
			return true;//RxCommand();
		}
		
		public void setTempControlTemp(ushort channel, short temperature)
		{
			TxCommand(DaqCommand.TEMP_CONTROL_SET_TEMP, Serializer.serializeParams(channel, (short)(temperature*100)));
		}
		
		public void SetTempControlCalOffset(ushort channel, short temperatureX10)
		{
			TxCommand(DaqCommand.TEMP_CONTROL_WRITE_CAL_OFFSET, Serializer.serializeParams(channel, (short)(temperatureX10*10)));
		}

		public void setTempControlPidGain(short PGain, short IGain, short DGain)
		{
			TxCommand(DaqCommand.TEMP_CONTROL_WRITE_PID_PARAMS, Serializer.serializeParams(PGain, IGain, DGain ));
		}
		
		public void GetTemperatureData()
		{
			SendCommand(DaqCommand.TEMP_CONTROL_READ, new byte[] {});
		}
		
		public bool IsConnected
		{
			get {
				return commsPort.IsOpen;
			}
		}
		
		override public bool checkConnection()
		{
			return IsConnected;
		}

		
		public bool SendCommand(DaqCommand command, byte[] data)
		{
			commsPort.DiscardInBuffer();
			TxCommand(command, data);
			return true;//RxCommand();
		}
		
		public bool SendCommand(DaqCommand command, params object[] paramlist)
		{
			byte[] args = Serializer.serializeParams(paramlist);
			return SendCommand(command, args);
		}
		
		private double daqReference = 5.0;
		public double DaqReference {
			get { return daqReference; }
			set { daqReference = value; }
		}

		public void DaqWrite(byte channel, double voltage)
		{
			ushort count = (ushort)System.Math.Max(DaqReference,((voltage / DaqReference) * 4095));
			SendCommand(DaqCommand.DAC_WRITE, channel, count);
		}
		
		override public void SetDigOut(byte channel, bool value)
		{
			SendCommand(DaqCommand.DIG_OUTPUT_WRITE, channel, value);
		}

		override public Control GetConfigControl()
		{
			return LeighDaqControl;
		}
		
		private LeighDaqControl leighDaqControl;
		public LeighDaqControl LeighDaqControl {
			get {
				if(leighDaqControl==null) {
					leighDaqControl = new LeighDaqControl();
					leighDaqControl.LeighDAQ = this;
				}
				return leighDaqControl;
			}
		}
		
		public void ForceCPUReset()
		{
			SendCommand(DaqCommand.REBOOT, new byte[0]);
		}
	}
}
