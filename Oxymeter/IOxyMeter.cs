﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 18/03/2010
 * Time: 11:33 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace LeighOxymeter
{
	/// <summary>
	/// Description of IOxyMeter.
	/// </summary>
	
	public interface IOxymeterDataRecord{
		
		DateTime RecievedAt
		{
			get;
		}

		List<double> O2 {
			get;
		}
		double Temperature {
			get;
		}
	}
	
	public delegate void RecievedO2measurementEventHandler(IOxymeterDataRecord oxymeterDataRecord);
	
	public interface IOxyMeter
	{
		void Open();
		string Address {
			set;
			get;
		}
		void Close();
		string[] GetAvailablePorts();
		event RecievedO2measurementEventHandler OnRecievedO2measurement;
		void Initialise(Control syncControl);
		bool IsReady {
			get;
		}
		int ChannelsAvailable {
			get;
		}
		int ChannelsEnabled {
			get;
			set;
		}
	}
}
