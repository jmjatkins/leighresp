﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 18/03/2010
 * Time: 3:14 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace LeighOxymeter
{
	/// <summary>
	/// Description of OxyMeterServiceControl.
	/// </summary>
	public partial class OxyMeterServiceControl : UserControl
	{
		public OxyMeterServiceControl()
		{
			InitializeComponent();
		}
		
		LeighOxymeter.OxyMeterService.OxyMeterTypes meterType;
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			meterType = (OxyMeterService.OxyMeterTypes)Enum.Parse(typeof(OxyMeterService.OxyMeterTypes), comboBox1.Text);
			OxyMeterService.instance.OxyMeterType = meterType;
			oxymeterControl1.Initialise();
		}
		
		void OxymeterControl1Load(object sender, EventArgs e)
		{
			comboBox1.DataBindings.Add(new Binding("text", OxyMeterService.instance,"OxyMeterType", false));
			oxymeterControl1.Initialise();
		}
	}
}
