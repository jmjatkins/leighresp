/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 7/09/2009
 * Time: 1:32 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;
using System.Collections.Generic;



namespace LeighOxymeter
{
	/// <summary>
	/// Description of microx.
	/// </summary>
	public class Microx : IOxyMeter
	{
		SerialPort sPort;
		bool gotNewReading = false;
		public Microx(  )
		{
			sPort = new SerialPort();
			sPort.BaudRate = 19200;
			sPort.DtrEnable = false;
			sPort.RtsEnable = false;
			sPort.ReadTimeout = 2000;
			sPort.DataReceived += RxInterrupt;
		}

		public int ChannelsAvailable {
			get {
				return 1;
			}
		}
		
		public int ChannelsEnabled {
			get {
				return 1;
			}
			set {
			}
		}
		
		string address = "COM1";
		public string Address {
			get { return address; }
			set { address = value; }
		}
		
		public void Open()
		{
			if(sPort.IsOpen) sPort.Close();
			sPort.PortName = Address;
			sPort.Open();
		}

		void RxInterrupt(object sender, SerialDataReceivedEventArgs e)
		{
			try {
				string rxData = sPort.ReadLine();
				sPort.DiscardInBuffer();
				if(rxData[0] != '@') ProcessDataPacket(rxData);
			}
			catch
			{
			}
		}
		
		public string[] GetAvailablePorts()
		{
			return	SerialPort.GetPortNames();
		}
		
		private void ProcessDataPacket(string rxData)
		{
			OxymeterDataRecord omr = new OxymeterDataRecord();
			omr.RecievedAt = DateTime.Now;
			try {
				int start = rxData.IndexOf('O') + 1;
				int fin = rxData.IndexOf(';', start);
				string sub = rxData.Substring(start, fin - start);
				omr.O2 = new  List<double> { Convert.ToDouble(  sub ) / 100 };
				start = rxData.IndexOf('T') + 1;
				fin = rxData.IndexOf(';', start);
				sub = rxData.Substring(start, fin - start);
				omr.Temperature = Convert.ToDouble(  sub ) / 10;
				gotNewReading = true;
			}
			catch
			{
			}
			if(recievedO2measurementEvent!=null) recievedO2measurementEvent(omr);
		}
		
		private RecievedO2measurementEventHandler recievedO2measurementEvent;
		public event RecievedO2measurementEventHandler OnRecievedO2measurement {
			add {recievedO2measurementEvent+=value;}
			remove {recievedO2measurementEvent-=value;}
		}
		
		void TXData(string buf)
		{
			TXData(buf, 8);
		}

		void TXData(string buf, int delay)
		{
			if(sPort.IsOpen)
			{
				sPort.DiscardInBuffer();
				for(int i=0; i< buf.Length; i++)
				{
					sPort.Write(new char[] {buf[i]}, 0, 1);
					Thread.Sleep(delay);
				}
			}
		}
		
		public void Initialise(Control syncControl)
		{
			if(!sPort.IsOpen) sPort.Open();
			TXData("\r", 330);
			TXData("mode0000\r");
			
			Thread.Sleep(500);

			TXData("\r", 330);
			TXData("mode0000\r");

			Thread.Sleep(500);

			TXData("\r", 330);
			TXData("mode0000\r");

			Thread.Sleep(500);
			TXData("\r", 330);
			TXData("mode0000\r");

			TXData("pwrc0001\r");
			TXData("sens0001\r");

			Thread.Sleep(500);
			TXData("samp0001\r");
		}
		
		public bool IsReady {
			get { return sPort.IsOpen && gotNewReading; }
		}
		
		public void Close()
		{
			if(sPort.IsOpen)
				sPort.Close();
		}
		
		
		public void GetReport()
		{
			TXData("repo\r");
		}
		
		public void GetData()
		{
			TXData("data\r");
		}
	}
}
