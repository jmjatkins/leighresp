﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 10/11/2011
 * Time: 9:30 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Timers;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;

namespace LeighOxymeter
{
	/// <summary>
	/// Description of FireSting.
	/// </summary>

	public class FireSting : IOxyMeter
	{
		const int maxChannels = 4;
		
		SerialPort sPort;
		List<double> O2;
		bool gotNewReading = false;
		double tempr = 0;

		System.Timers.Timer timer = new System.Timers.Timer(5000);

		public FireSting()
		{
			O2= new List<double> {0,0,0,0};
			sPort = new SerialPort();
			sPort.BaudRate = 19200;
			sPort.DtrEnable = false;
			sPort.RtsEnable = false;
			sPort.ReadTimeout = 1000;
			sPort.DataReceived += RxData;
			rxDataDelegate = new MethodInvoker(RxInterrupt);
			timer.Elapsed += new ElapsedEventHandler( OnTimedEvent );
			timer.Start();
		}
		
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			timer.Enabled = false;
			try {
				if((DateTime.Now - lastRxPacket).Seconds > 10 )
					runStateMachine();
			}
			finally {
				timer.Enabled = true;
			}
		}
		
		public int ChannelsAvailable {
			get {
				return 2;
			}
		}
		
		int channelsEnabled = 2;
		public int ChannelsEnabled {
			get { return channelsEnabled; }
			set {
				if(value > ChannelsAvailable) value = ChannelsAvailable;
				channelsEnabled = value;
			}
		}
		
		
		private RecievedO2measurementEventHandler recievedO2measurementEvent;
		public event RecievedO2measurementEventHandler OnRecievedO2measurement {
			add {recievedO2measurementEvent+=value;}
			remove {recievedO2measurementEvent-=value;}
		}
		
		string address = "COM1";
		public string Address {
			get { return address; }
			set { address = value; }
		}
		
		public bool IsReady {
			get { return sPort.IsOpen && gotNewReading; }
		}
		
		public void Open()
		{
			if(sPort.IsOpen) sPort.Close();
			sPort.PortName = Address;
			sPort.Open();
		}
		
		public void Close()
		{
			if(sPort.IsOpen)
				sPort.Close();
		}
		
		public string[] GetAvailablePorts()
		{
			return	SerialPort.GetPortNames();
		}
		
		public void Initialise( Control syncControl )
		{
			this.sycnControl = syncControl;
			if(sPort.IsOpen) {
				sPort.Write("RMR 1 3 0 7");
			}
		}
		
		
		DateTime lastRxPacket;
		void RxInterrupt()
		{
			lastRxPacket = DateTime.Now;
			string rxData = sPort.ReadTo("\r");
			if(rxData.Contains("RMR")) {
				ProcessDataPacket(rxData);
			}
			runStateMachine();
		}


		static int channel = 1;
		enum FIRESTING_STATE { SEND_MSR, SEND_TMP, SEND_RMR };
		FIRESTING_STATE state = FIRESTING_STATE.SEND_MSR;
		
		private void runStateMachine()
		{
			string s;
			if(sPort.IsOpen) {
				
				switch(state) {
					case FIRESTING_STATE.SEND_MSR:
						//s = String.Format( "MSR {0}\rMSR {0}\r;", channel);
						s = String.Format( "MSR {0}\r", channel);
						sPort.Write(s);
						if(++channel > ChannelsEnabled) {
							channel = 1;
							state++;
						}
						break;
					case FIRESTING_STATE.SEND_TMP:
						//sPort.Write("TMP 1\rTMP 1\r");
						sPort.Write("TMP 1\r");
						state++;
						break;
					case FIRESTING_STATE.SEND_RMR:
						//s = String.Format( "RMR {0} 3 0 7\nRMR {0} 3 0 7\r", channel);
						s = String.Format( "RMR {0} 3 0 7\r", channel);
						sPort.Write(s);
						if(++channel > ChannelsEnabled) {
							channel = 1;
							state = FIRESTING_STATE.SEND_MSR;
						}
						break;
				}
			}
		}
		
		Control sycnControl;
		public void SetSycnControl(Control sycnControl)
		{
			this.sycnControl = sycnControl;
		}
		
		MethodInvoker rxDataDelegate;
		void RxData(object sender, EventArgs e)
		{
			if(sycnControl !=null && !sycnControl.IsDisposed && !sycnControl.Disposing) {
				lock(this)
				{
					try {
						sycnControl.Invoke(rxDataDelegate);
					}
					catch {
						
					}
				}
			}
		}
		
		private void SendData()
		{
			OxymeterDataRecord omr = new OxymeterDataRecord();
			omr.RecievedAt = DateTime.Now;
			omr.O2 = O2.GetRange(0, ChannelsEnabled );
			omr.Temperature = tempr;
			try {
				if(recievedO2measurementEvent!=null) recievedO2measurementEvent(omr);
			}
			catch {
			}
		}
		
		private void ProcessDataPacket(string rxData)
		{
			try {
				string[] s = rxData.Split(' ');
				int channel = Convert.ToInt16( s[1] );
				O2[ channel - 1 ] = Convert.ToDouble( s[9] ) / 1000;
				tempr = Convert.ToDouble( s[10] ) / 1000;
				gotNewReading = true;
				if(channel == 1) SendData();
			}
			catch {
			}
		}
		
	}
}
