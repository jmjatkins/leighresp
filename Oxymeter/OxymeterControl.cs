﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 18/03/2010
 * Time: 10:22 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace LeighOxymeter
{
	/// <summary>
	/// Description of Oxymeter3310Control.
	/// </summary>
	public partial class OxymeterControl : UserControl
	{
		List<double> LastO2 = new List<double>();
		double LastTemperature;
		IOxyMeter oxymeter;

		public OxymeterControl()
		{
			InitializeComponent();
		}

		public void Initialise()
		{
			if(this.oxymeter != OxyMeterService.instance)
			{
				if(this.oxymeter !=null) {
					this.oxymeter.Close();
					comboBox1.DataBindings.Clear();
				}
				this.oxymeter = OxyMeterService.instance;
				comboBox1.Text = oxymeter.Address;
				textBox3.DataBindings.Clear();
				textBox3.DataBindings.Add("Text", oxymeter, "ChannelsEnabled");
			}
		}
		
		
		void Button1Click(object sender, EventArgs e)
		{
		}
		
		void ComboBox1DropDown(object sender, EventArgs e)
		{
			comboBox1.Items.Clear();
			comboBox1.Items.AddRange(oxymeter.GetAvailablePorts());
		}
		
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}
		
		void OxymeterControlLoad(object sender, EventArgs e)
		{
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			if(oxymeter!=null)
			{
				try {
					if(oxymeter.Address != comboBox1.Text) {
						oxymeter.Address = comboBox1.Text;
						oxymeter.Open();
						oxymeter.Initialise(this);
					}
				}
				catch
				{
				}
			}
			this.ParentForm.Close();
		}
	}
}
