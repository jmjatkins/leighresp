﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 18/03/2010
 * Time: 2:22 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;

namespace LeighOxymeter
{
	/// <summary>
	/// Description of OxyMeterFactory.
	/// </summary>
	public class OxyMeterService : IOxyMeter
	{
		
		static public OxyMeterService instance = new OxyMeterService();
		
		OxyMeterService()
		{
			try {
				OxyMeterType = (OxyMeterService.OxyMeterTypes)Enum.Parse( typeof(OxyMeterService.OxyMeterTypes) , XmlAppSettings.Settings.GetSetting("OxyMeterType", "Oxy3310") );
				Address = XmlAppSettings.Settings.GetSetting("OxyMeterAddress", "COM6");
				ChannelsEnabled = XmlAppSettings.Settings.GetSetting("ChannelsEnabled", 1);
			}
			catch {
				
			}
		}
		
		IOxyMeter oxyMeter = new Oxy3310();
		
		internal IOxyMeter OxyMeter {
			set {
				if(oxyMeter!=null)
					oxyMeter.OnRecievedO2measurement -= OnRecievedO2;
				oxyMeter = value;
				oxyMeter.OnRecievedO2measurement+= OnRecievedO2;
				XmlAppSettings.Settings.PutSetting( "OxyMeterType",  GetOxymeterType(oxyMeter).ToString() );
				oxyMeter.ChannelsEnabled = this.ChannelsEnabled;
			}
			get {
				return oxyMeter;
			}
		}
		
		void OnRecievedO2(IOxymeterDataRecord oxymeterDataRecord) {
			if(recievedO2measurementEvent!=null) recievedO2measurementEvent(oxymeterDataRecord);
		}
		
		public string[] GetAvailablePorts()
		{
			return oxyMeter.GetAvailablePorts();
		}
		
		public string Address {
			get { return oxyMeter.Address; }
			set {
				oxyMeter.Address = value;
				XmlAppSettings.Settings.PutSetting( "OxyMeterAddress", value);
			}
		}

		public int ChannelsAvailable {
			get {
				return oxyMeter.ChannelsAvailable;
			}
		}
		
		public int ChannelsEnabled {
			get { return oxyMeter.ChannelsEnabled; }
			set {
				oxyMeter.ChannelsEnabled = value;
				XmlAppSettings.Settings.PutSetting( "ChannelsEnabled",  ChannelsEnabled.ToString() );
			}
		}
		
		
		
		public void Open()
		{
			oxyMeter.Open();
		}

		public void Initialise( Control syncControl )
		{
			Open();
			oxyMeter.Initialise(syncControl);
		}
		
		public void Close()
		{
			oxyMeter.Close();
		}
		
		public bool IsReady {
			get {
				return oxyMeter.IsReady;
			}
		}
		
		private RecievedO2measurementEventHandler recievedO2measurementEvent;
		public event RecievedO2measurementEventHandler OnRecievedO2measurement {
			add {recievedO2measurementEvent+=value;}
			remove {recievedO2measurementEvent-=value;}
		}
		
		OxyMeterServiceControl control;
		public OxyMeterServiceControl Control {
			get {
				if(control==null)
				{
					control = new OxyMeterServiceControl();
				}
				return control;
			}
		}
		
		public enum OxyMeterTypes{ Microx, Oxy3310, FireSting };
		
		OxyMeterTypes oxyMeterType;
		
		public OxyMeterTypes OxyMeterType {
			get { return oxyMeterType; }
			set {
				if(oxyMeterType != value) {
					oxyMeterType = value;
					OxyMeter.Close();
					switch(oxyMeterType)
					{
							case OxyMeterTypes.Microx: OxyMeter = new Microx(); break;
							case OxyMeterTypes.Oxy3310: OxyMeter = new Oxy3310(); break;
							case OxyMeterTypes.FireSting: OxyMeter = new FireSting(); break;
					}
				}
			}
		}
		
		
		public OxyMeterTypes GetOxymeterType(IOxyMeter meter){
			if(meter is Oxy3310) return OxyMeterTypes.Oxy3310;
			else if(meter is FireSting) return OxyMeterTypes.FireSting;
			else return OxyMeterTypes.Microx;
		}
	}
}
