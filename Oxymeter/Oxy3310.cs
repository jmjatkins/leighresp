﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 18/03/2010
 * Time: 10:13 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO.Ports;
using System.Windows.Forms;
using System.Collections.Generic;
namespace LeighOxymeter
{
	/// <summary>
	/// Interface class for Oxy3310 Oxygen Meter
	/// </summary>

	public class OxymeterDataRecord : IOxymeterDataRecord
	{
		DateTime recievedAt;
		public DateTime RecievedAt {
			get { return recievedAt; }
			set { recievedAt = value; }
		}

		List<double> o2 = new List<double> {-999};
		public List<double> O2 {
			get { return o2; }
			set { o2 = value; }
		}
		
		double temperature = -999;
		public double Temperature {
			get { return temperature; }
			set { temperature = value; }
		}
	}
	
	public class Oxy3310 : IOxyMeter
	{
		SerialPort sPort;
		bool gotNewReading = false;

		public Oxy3310()
		{
			sPort = new SerialPort();
			sPort.BaudRate = 19200;
			sPort.DtrEnable = false;
			sPort.RtsEnable = false;
			sPort.ReadTimeout = 2000;
			sPort.DataReceived += RxData;
			rxDataDelegate = new MethodInvoker(RxInterrupt);
		}
		
		public int ChannelsAvailable {
			get {
				return 1;
			}
		}
		
		public int ChannelsEnabled {
			get{ return 1; }
			set {}
		}

		public string[] GetAvailablePorts()
		{
			return	SerialPort.GetPortNames();
		}

		string address = "COM1";
		public string Address {
			get { return address; }
			set { address = value; }
		}

		public void Open()
		{
			if(sPort.IsOpen) sPort.Close();
			sPort.PortName = Address;
			sPort.Open();
		}

		public void Close()
		{
			if(sPort.IsOpen) sPort.Close();
		}
		
		public bool IsReady {
			get { return sPort.IsOpen && gotNewReading; }
		}

		void RxInterrupt()
		{
			try {
				string rxData = sPort.ReadLine();
				sPort.DiscardInBuffer();
				if(rxData.Contains("Oxi 3310")) ProcessDataPacket(rxData);
			}
			catch
			{
			}
		}
		
		public void Initialise( Control syncControl )
		{
			this.sycnControl = syncControl;
			if(sPort.IsOpen) {
				sPort.Write("RMR 1 3 0 7");
			}
		}

		Control sycnControl;
		public void SetSycnControl(Control sycnControl)
		{
			this.sycnControl = sycnControl;
		}
		
		MethodInvoker rxDataDelegate;
		void RxData(object sender, EventArgs e)
		{
			if(sycnControl !=null && !sycnControl.IsDisposed && !sycnControl.Disposing) {
				lock(this)
				{
					try {
						sycnControl.Invoke(rxDataDelegate);
					}
					catch {
						
					}
				}
			}
		}

		
		private void ProcessDataPacket(string rxData)
		{
			OxymeterDataRecord omr = new OxymeterDataRecord();
			omr.RecievedAt = DateTime.Now;
			try {
				//Oxi 3310;10040292;1;18.03.2010;10:54:17;92.1;;;25.6;;AR;++
				//Oxi 3310; 10040292; 1; 18.03.2010; 10:54:17; 92.1; ; ; 25.6; ; AR; ++
				//0			1		  2   3			  4			5   6 7    8  9   10
				string[] s = rxData.Split(';');
				omr.O2 = new List<double> { Convert.ToDouble( s[5] ) };
				omr.Temperature =  Convert.ToDouble( s[8] );
				gotNewReading = true;
			}
			catch
			{
				
			}
			if(recievedO2measurementEvent!=null) recievedO2measurementEvent(omr);
		}

		private RecievedO2measurementEventHandler recievedO2measurementEvent;
		public event RecievedO2measurementEventHandler OnRecievedO2measurement {
			add {recievedO2measurementEvent+=value;}
			remove {recievedO2measurementEvent-=value;}
		}
		
	}
}
