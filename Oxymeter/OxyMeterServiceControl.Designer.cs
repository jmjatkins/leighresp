﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 18/03/2010
 * Time: 3:14 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LeighOxymeter
{
	partial class OxyMeterServiceControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.oxymeterControl1 = new LeighOxymeter.OxymeterControl();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
									"Microx",
									"Oxy3310",
									"FireSting"});
			this.comboBox1.Location = new System.Drawing.Point(67, 28);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(122, 21);
			this.comboBox1.TabIndex = 0;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// oxymeterControl1
			// 
			this.oxymeterControl1.Location = new System.Drawing.Point(22, 65);
			this.oxymeterControl1.Name = "oxymeterControl1";
			this.oxymeterControl1.Size = new System.Drawing.Size(208, 161);
			this.oxymeterControl1.TabIndex = 1;
			this.oxymeterControl1.Load += new System.EventHandler(this.OxymeterControl1Load);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(22, 31);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(39, 18);
			this.label1.TabIndex = 2;
			this.label1.Text = "Type";
			// 
			// OxyMeterServiceControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.oxymeterControl1);
			this.Controls.Add(this.comboBox1);
			this.Name = "OxyMeterServiceControl";
			this.Size = new System.Drawing.Size(256, 248);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label1;
		private LeighOxymeter.OxymeterControl oxymeterControl1;
		private System.Windows.Forms.ComboBox comboBox1;
	}
}
