﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 18/03/2010
 * Time: 10:13 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LeighOxymeter
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.oxymeter3310Control1 = new LeighOxymeter.OxymeterControl();
			this.SuspendLayout();
			// 
			// oxymeter3310Control1
			// 
			this.oxymeter3310Control1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.oxymeter3310Control1.Location = new System.Drawing.Point(0, 0);
			this.oxymeter3310Control1.Name = "oxymeter3310Control1";
			this.oxymeter3310Control1.Size = new System.Drawing.Size(198, 355);
			this.oxymeter3310Control1.TabIndex = 0;
			this.oxymeter3310Control1.Load += new System.EventHandler(this.Oxymeter3310Control1Load);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(198, 355);
			this.Controls.Add(this.oxymeter3310Control1);
			this.Name = "MainForm";
			this.Text = "Oxymeter3310";
			this.ResumeLayout(false);
		}
		private LeighOxymeter.OxymeterControl oxymeter3310Control1;
	}
}
