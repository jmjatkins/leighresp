/*
 * Created by SharpDevelop.
 * User: John
 * Date: 30/08/2007
 * Time: 11:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;

namespace Math
{
	/// <summary>
	/// Regression.
	/// </summary>
	public static class  Regression
	{
		static public void PolyRegression(Matrix X, double[] y, 
		                           out double[] coeff, out double[] yFit, out double error, out double r_square)
		{
			Matrix D = X.MTranspose();
			Matrix DI = D.Invert();
			double[] xy = Matrix.MultMV( X, y);
			coeff = Matrix.MultMV(DI, xy);
			yFit = Matrix.MultMTV(X, coeff);
			error = Matrix.RMS(y, yFit);
			double ss = Matrix.SS(y);
			r_square = ss == 0 ? 0 : 1 - ((error*error*y.Length) / ss);
		}
		
		
		static Matrix FormXPowers(double[] xV, int ndegree)
		{
			Matrix result  = new Matrix(ndegree,xV.Length);
			for (int j = 0; j < xV.Length; j++)
			{
				double s = 1.0;
				double t = xV[j];
				for (int i = 0; i < ndegree; i++)
				{
					result[i,j] = s;
					s *= t;
				}
			}
			return result;
		}
		
		static public void LinearRegression( double[] x, double[] y, out double c0,
		                             out double c1, out double[] yFit, out double error, out double r_square)
		{
			double[] coeff = new double[2];
			Matrix XM = FormXPowers(x, 2);
			PolyRegression(XM, y, out coeff, out yFit, out error, out r_square);
			c0 = coeff[0];
			c1 = coeff[1];
		}
	}
}
