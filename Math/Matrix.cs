/*
 * ${res:XML.StandardHeader.CreatedBySharpDevelop}
 * ${res:XML.StandardHeader.User} ${USER}
 * ${res:XML.StandardHeader.Date} ${DATE}
 * ${res:XML.StandardHeader.Time} ${TIME}
 * 
 * ${res:XML.StandardHeader.HowToChangeTemplateInformation}
 */
using System;
using System.Collections.Generic;
namespace Math
{
	
	/// <summary>
	/// OST Matrix Class
	/// </summary>
	public class Matrix
	{
		double[,] _data;
		public Matrix(int m, int n) {_data = new double[m,n];}
		public Matrix(double[,] array)	{_data = array;}
		public double this[int i,int j] {
			get {return _data[i,j];}
			set {_data[i,j]=value;}
		}
		public int M {get { return _data.GetLength(0);}}
		public int N {get { return _data.GetLength(1);}}

		
		public Matrix MTranspose()
		{
			Matrix result = new Matrix(M,M);
			for (int i = 0; i < M; i++)
			{
				for (int j = 0; j < M; j++)
				{
					double sum = 0.0;
					for (int k = 0; k < N; k++)
						sum += (this[i,k] * this[j,k]);
					result[i,j] = sum;
				}
			}
			return result;
		}
		
		
		
		public void Triangulate()
		{
			double xt;

			for (int n = 0; n < M-1; n++)
			{
				// Select pivot
				double x = System.Math.Abs(this[n,n]);
				int pivot = n; // Initial assumption
				for (int row = n + 1; row < M; row++)
				{
					if ((xt = System.Math.Abs(this[row,n])) > x)
					{
						// Best so far
						x = xt;
						pivot = row;
					}
				}
				if (pivot != n)
				{
					for (int col = 0; col < N; col++)
					{   double swap = this[n,col];
						this[n,col] = this[pivot,col];
						this[pivot,col] = -swap;
					}
				}
				if (x > 0.00001)
				{
					for (int row = n + 1; row < M; row++)
					{
						double s = this[row,n] / this[n,n];
						this[row,n] = 0.0;
						for (int col = n + 1; col < N; ++col)
							this[row,col] -=  s * this[n,col];
					}
				}
				else throw new ApplicationException("Triangulate Failed");
			}
		}
		
		public Matrix Invert()
		{
			// Working array, twice the width of S.  The left-hand
			// half contains S, and the inverse is formed in the
			// right-hand half.
			if(M!=N) throw new ApplicationException("Matrix Must Be Square To Invert");
			int size = M;
			int     n = 2 * size;
			Matrix W = new Matrix(size, n);
			
			// Copy S into LHS of W, setting RHS of W to unit matrix.
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < size; j++)
				{
					W[i,j] = this[i,j];
					W[i,j + size] = 0.0;
				}
				W[i,i + size] = 1.0;
			}
			
			// Attempt to lose upper triangle.
			W.Triangulate();
			for (int i = size - 1; i >= 0; i--)
			{
				// Normalise diagonal elements
				double u = 1.0 / W[i,i];
				for (int j = size; j < n; j++)
					W[i,j] *= u;
				for (int k = i - 1; k >= 0; k--)
				{
					u = W[k,i];
					for (int j = size; j < n; j++)
						W[k,j] -= u * W[i,j];
				}
			}
			
			Matrix Result = new Matrix(size,size);
			// Copy RHS of W into D.
			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					Result[i,j] = W[i,j + size];
			
			return Result;
		}
		
		public static double[] MultMV(Matrix A, double[] v )
		{
			double[] result = new double[A.M];
			for (int i = 0; i < A.M; i++)
			{
				double sum = 0.0;
				for (int j = 0; j < A.N; j++)
					sum += (A[i,j] * v[j]);
				result[i] = sum;
			}
			return result;
		}
		
		public static double[] MultMTV(Matrix A, double[] v)
		{
			double[] result = new double[A.N];
			for (int j = 0; j < A.N; j++)
			{
				double sum = 0.0;
				for (int i = 0; i < A.M; i++)
					sum += (A[i,j] * v[i]);
				result[j] = sum;
			}
			return result;
		}

		public static double RMS(double[] y, double[] yy)
		{
			double r;          // temp
			double ss = 0.0;   // sum of squares
			
			for (int i = 0; i < y.Length; i++)
			{
				r = y[i] - yy[i];
				ss += (r * r);
			}
			return System.Math.Sqrt(ss / y.Length);
		}
		
		
		public static double Mean(double[] y)
		{
			if(y.Length==0) return 0;
			double r =  0.0;          // temp
			for (int i = 0; i < y.Length; i++)
			{
				r += y[i];
			}
			return r / y.Length;
		}
		
		
		public static double SS(double[] y)
		{
			
			double m = Mean(y);
			double ss = 0.0;   // sum of squares
			
			for (int i = 0; i < y.Length; i++)
			{
				double e = y[i] - m;
				ss += e*e;
			}
			return ss;
		}
		
		
	}
}
